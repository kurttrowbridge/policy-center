<?php

namespace Drupal\briefing_center_top_story_access\NodeAccess;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides access control for top story nodes.
 */
class TopStoryNodeAccess {

  /**
   * Implements hook_node_access().
   */
  public function nodeAccess(EntityInterface $node, $operation, AccountInterface $account) {
    // Only care about viewing News nodes for anonymous users.
    if ($operation !== 'view' || 
        $node->bundle() !== 'news' || 
        !$account->isAnonymous()) {
      return AccessResult::neutral();
    }

    // Only open access to News nodes that have the Press & Media subject.
    $press_media_tid = 1694;
    if (!$node->hasField('field_subject')) {
      return AccessResult::neutral();
    }
    $subject_values = $node->get('field_subject')->getValue();
    $subject_ids = array_column($subject_values, 'target_id');
    if (!in_array($press_media_tid, $subject_ids)) {
      return AccessResult::neutral();
    }

    // Allow access to the News item if it's marked as a top story.
    if ($node->hasField('field_top_story') && $node->get('field_top_story')->value) {
      return AccessResult::allowed()
        ->addCacheableDependency($node)
        ->addCacheContexts(['user.roles'])
        ->addCacheTags(['node:' . $node->id()]);
    }

    return AccessResult::neutral()
      ->addCacheableDependency($node)
      ->addCacheContexts(['user.roles']);
  }

}
