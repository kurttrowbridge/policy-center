(function ($) {
    Drupal.behaviors.ajaxView = {
        attach: function (context, settings) {
            // Attach ajax action click event of each view row.
            $('.block-policycenter-content .views-row').once('attach-links').each(this.attachLink);
        },

        attachLink: function (idx, row) {

            // Dig out the node id from the header link.
            var nid = $(row).find('article').attr('data-history-node-id');
            console.log(row);
            console.log(nid);

            // Everything we need to specify about the view.
            var view_info = {
                view_name: 'stories',
                view_display_id: 'story_embed',
                view_args: nid,
                view_dom_id: 'ajax-view'
            };

            // Details of the ajax action.
            var ajax_settings = {
                submit: view_info,
                url: '/views/ajax',
                element: row,
                wrapper: 'ajax-view',
                method: 'append',
                event: 'click',
                progress: {
                    'type': 'throbber',
                    'message': Drupal.t('Loading story...')
                }
            };

            Drupal.ajax(ajax_settings);
        }
    };
})(jQuery);