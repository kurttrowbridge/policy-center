<?php
 
namespace Drupal\policycenter_ajax_stories\Plugin\Block;
 
use Drupal\Core\Block\BlockBase;
 
/**
 * Provides a 'AjaxViewBlock' block.
 *
 * @Block(
 *  id = "ajax_view_block",
 *  admin_label = @Translation("AJAX view block"),
 * )
 */
class AjaxViewBlock extends BlockBase {
 
  /**
   * {@inheritdoc}
   */
  public function build() {
 
    $build = [];
 
    $build['ajax_view_block'] = [
      '#theme' => 'ajax_view',
      '#attached' => ['library' => 'policycenter_ajax_stories/ajax-view'],
    ];
 
    return $build;
  }
 
}