<?php

namespace Drupal\policycenter_migrate\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Proxy API for Feedly.
 *
 * @package Drupal\policycenter_migrate\Controller
 */
class FeedlyProxyController extends ControllerBase {

  /**
   * Module constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Calls the Feedly API for data. Request is base64-encoded.
   *
   * @param string $request
   *   Grabs request params.
   */
  public function get($request) {
    // Get API base URL and access token for Feedly out of config.
    $api_base_url = $this->configFactory->get('policycenter_migrate.feedly_credentials')->get('api_base_url');
    $access_token = $this->configFactory->get('policycenter_migrate.feedly_credentials')->get('access_token');

    // Convert request URL from base64.
    $decodedUrl = base64_decode(str_replace('|', '/', $request));

    $token_curl = curl_init();

    // Prepare body of refresh token request.
    $authDetails = [
      "refresh_token" => "$access_token",
      "grant_type" => "refresh_token",
      "client_id" => "feedlydev",
      "client_secret" => "feedlydev",
    ];

    // Request refresh token.
    curl_setopt_array($token_curl, [
      CURLOPT_URL => $api_base_url . 'auth/token',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => http_build_query($authDetails),
      CURLOPT_HTTPHEADER => [
        'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
      ],
    ]);

    $tokenJsonResponse = curl_exec($token_curl);

    // Decode the response and get its access token.
    $decodedToken = json_decode($tokenJsonResponse);
    $refresh_token = $decodedToken->access_token;

    curl_close($token_curl);

    // Make authenticated request using the resulting token.
    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => $api_base_url . $decodedUrl,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $refresh_token,
      ],
    ]);

    $jsonResponse = curl_exec($curl);

    curl_close($curl);

    $response = JsonResponse::fromJsonString($jsonResponse);
    return $response;
  }

}
