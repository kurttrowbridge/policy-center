<?php

namespace Drupal\policycenter_migrate\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * The Feedly credential form.
 */
class FeedlyCredentialsForm extends ConfigFormBase {

  /**
   * Credentials constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() : array {
    return ['policycenter_migrate.feedly_credentials'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'policycenter_migrate_feedly_credentials';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) : array {
    $form['api_base_url'] = [
      '#title' => $this->t('API Base URL'),
      '#type' => 'textfield',
      '#default_value' => $this->config('policycenter_migrate.feedly_credentials')->get('api_base_url'),
      '#description' => $this->t('The base URL for the Feedly API.'),
      '#required' => TRUE,
    ];
    $form['access_token'] = [
      '#title' => $this->t('Access token'),
      '#type' => 'textarea',
      '#default_value' => $this->config('policycenter_migrate.feedly_credentials')->get('access_token'),
      '#description' => $this->t('The access token for the Feedly developer API. Must be manually generated every 30 days <a href="@feedly-access-token">here</a>.', [
        '@feedly-access' => 'https://cloud.feedly.com/v3/auth/dev',
      ]),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $api_base_url = $form_state->getValue('api_base_url');
    $access_token = $form_state->getValue('access_token');
    $config = $this->config('policycenter_migrate.feedly_credentials');
    $config->set('api_base_url', $api_base_url);
    $config->set('access_token', $access_token);
    $config->save();
  }

}
