<?php

namespace Drupal\policycenter_migrate\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Minimalistic example for a SqlBase source plugin.
 *
 * @MigrateSource(
 *   id = "news",
 *   source_module = "policycenter_migrate",
 * )
 */
class News extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'newswebhose' table.
    $query = $this->select('newswebhose', 'g')
      ->fields('g', [
          'recno',
          'articles__title',
          'articles__content',
          'articles__author',
          'heading',
          'articles__source__name',
          'articles__source__id',
          'articles__url',
          'source',
          'subject',
          'scraping_timestamp'
        ])
      ->condition('recno', 700000, '>=');
    $query->orderBy('recno');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'recno' => $this->t('recno'),
      'articles__title'        => $this->t('articles__title'),
      'articles__content'      => $this->t('articles__content'),
      'articles__author'       => $this->t('articles__author'),
      'heading'                => $this->t('heading'),
      'articles__source__name' => $this->t('articles__source__name'),
      'articles__source__id'   => $this->t('articles__source__id'),
      'articles__url'          => $this->t('articles__url'),
      'source'                 => $this->t('source'),
      'subject'                => $this->t('subject'),
      'scraping_timestamp'     => $this->t('scraping_timestamp'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'recno' => [
        'type' => 'integer',
        'alias' => 'g',
      ],
    ];
  }
}