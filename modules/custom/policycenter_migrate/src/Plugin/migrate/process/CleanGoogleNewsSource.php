<?php

namespace Drupal\policycenter_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Cleans up rows with a Google News search query as the source.
 *
 * @MigrateProcessPlugin(
 *   id = "clean_google_news_source"
 * )
 *
 * @code
 * field_date:
 *   plugin: clean_google_news_source
 *   source: news_source
 * @endcode
 */
class CleanGoogleNewsSource extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Only change the Source Name if it includes Google News.
    if (str_contains($value, 'Google News')) {
      // Get title field from migration row.
      $title = $row->getSourceProperty('title');
      if ($title) {
        // Get source from row's title.
        $original_source = explode('- ', $title);
        $value = $original_source ? end($original_source) : 'Google News';
      }
    }
    return $value;
  }

}
