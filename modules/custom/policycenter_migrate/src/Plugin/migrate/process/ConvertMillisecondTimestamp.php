<?php

namespace Drupal\policycenter_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Convert 13-digit timestamp with milliseconds to 10-digit timestamp.
 *
 * @MigrateProcessPlugin(
 *   id = "convert_millisecond_timestamp"
 * )
 *
 * @code
 * field_date:
 *   plugin: convert_millisecond_timestamp
 *   source: timestamp
 * @endcode
 */
class ConvertMillisecondTimestamp extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!is_numeric($value)) {
      throw new MigrateSkipRowException('Value is not numeric; skipping row.');
    }
    return floor($value / 1000);
  }

}
