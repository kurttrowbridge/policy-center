<?php

namespace Drupal\briefing_center_top_sources\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Briefing Center Top Sources settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The briefing_center_top_sources.settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->config = $instance->config('briefing_center_top_sources.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'briefing_center_top_sources_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['briefing_center_top_sources.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Gather the number of sources in the form already.
    $config_number_of_top_sources =
      $this->config('briefing_center_top_sources.settings')->get('top_sources') ?
      (count($this->config('briefing_center_top_sources.settings')->get('top_sources')) - 1)
      : 0;
    $number_of_top_sources = $form_state->get('number_of_top_sources') ?? $config_number_of_top_sources;
    // We have to ensure that there is at least one source field.
    if ($number_of_top_sources === NULL) {
      $form_state->set('number_of_top_sources', 1);
      $number_of_top_sources = 1;
    }
    else {
      $form_state->set('number_of_top_sources', $number_of_top_sources);
    }

    $form['info'] = [
      '#type' => 'item',
      '#markup' => '<p>List the names of sources whose articles should be automatically marked as Top Stories.</p>',
    ];

    $form['sources_fieldset'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => $this->t('Top sources'),
      '#prefix' => '<div id="sources-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $number_of_top_sources; $i++) {
      $form['sources_fieldset'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Source ' . $i + 1),
        '#default_value' => $this->config('briefing_center_top_sources.settings')->get('top_sources.' . $i),
      ];
    }

    $form['sources_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['sources_fieldset']['actions']['add_source'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another source'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'sources-fieldset-wrapper',
      ],
    ];
    // If there is more than one source, add the remove button.
    if ($number_of_top_sources > 1) {
      $form['sources_fieldset']['actions']['remove_source'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove last source'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'sources-fieldset-wrapper',
        ],
      ];
    }
 
    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the sources in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['sources_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $source_field = $form_state->get('number_of_top_sources');
    $add_button = $source_field + 1;
    $form_state->set('number_of_top_sources', $add_button);
    // Since our buildForm() method relies on the value of 'number_of_top_sources' to
    // generate 'source' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $source_field = $form_state->get('number_of_top_sources');
    if ($source_field > 1) {
      $remove_button = $source_field - 1;
      $form_state->set('number_of_top_sources', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'number_of_top_sources' to
    // generate 'source' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('briefing_center_top_sources.settings')
      ->set('top_sources', $values['sources_fieldset'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
