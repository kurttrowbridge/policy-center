<?php

/**
* @file
* Contains \Drupal\flag_trigger\EventSubscriber\FlagTrigger.
*/

namespace Drupal\flag_trigger\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\flag\Event\FlagEvents;
use Drupal\flag\Event\FlaggingEvent;

class FlagTrigger implements EventSubscriberInterface {

  public function onFlag(FlaggingEvent $event) {
    $flagging = $event->getFlagging();
    $entity = $flagging->getFlaggable();

    $type = $entity->getType();

    // if flagging a Topic with the Pin flag...
    if ($type == 'topic') {
      $account = \Drupal::currentUser();
      $flag_service = \Drupal::service('flag');
      $flagTB = $flag_service->getFlagById('topic_bar');
      $flagging = $flag_service->getFlagging($flagTB, $entity, $account);
      if ($event->getFlagging()->getFlag()->id() == 'pin') {
        if(!$flagging) {
          // ...also flag it as flag type Topic Bar
          $flag_service->flag($flagTB, $entity, $account);
        }
      }
    }
  }

  public static function getSubscribedEvents() {
    $events = [];
    $events[FlagEvents::ENTITY_FLAGGED][] = ['onFlag'];
    return $events;
  }

}