<?php

namespace Drupal\briefing_center_one_ai\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure One AI settings.
 */
class OneAISettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['briefing_center_one_ai.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'briefing_center_one_ai_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('briefing_center_one_ai.settings');

    $form['agent_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('One AI Agent ID'),
      '#default_value' => $config->get('agent_id'),
      '#description' => $this->t('Enter the One AI Agent ID for API integration.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('briefing_center_one_ai.settings')
      ->set('agent_id', $form_state->getValue('agent_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
