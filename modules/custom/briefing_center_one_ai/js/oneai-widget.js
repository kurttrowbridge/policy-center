((Drupal, drupalSettings) => {
  'use strict';

  Drupal.behaviors.briefingCenterOneAiWidget = {
    attach: function (context, settings) {
      if (context !== document) {
        return;
      }

      // Get agent ID from drupalSettings.
      const agentId = drupalSettings.briefingCenterOneAi.agentId;
      
      // Create and append the script.
      const script = document.createElement('script');
      script.id = 'oneai-snippet';
      script.src = `https://oneai.com/~widget?id=${agentId}`;
      document.body.appendChild(script);
    }
  };

})(Drupal, drupalSettings); 