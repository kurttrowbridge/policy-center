<?php
require 'vendor/autoload.php';
use MongoDB\Client;

define('APIKEY', '<YOUR API KEY>');

const CLIENT_CONNECTION_STRING =  "mongodb+srv://jd:MSQISqc3OYCdrDEB@cluster0.aslfl.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const CONNECTION_STRING = CLIENT_CONNECTION_STRING; // Change to CLIENT_CONNECTION_STRING for production
// Define the database and collection names
const DATABASE_NAME = "embedding-db";
const COLLECTION_NAME = "vector-collection";

function getHeaders() {
    return [
        "api-key: " . APIKEY,
        "output_type: json",
        "Content-Type: application/json",
    ];
}

function get_headline_body($Url) {
    return json_encode([
        "input" => $Url,
        "input_type" => "article",
        "steps" => [
            ["skill" => "html-extract-article"],
            ["skill" => "headline"],
        ],
    ]);
}

function get_headline($Url) {
    $headers = getHeaders();
    $body = get_headline_body($Url);
    $api_url = "https://api.oneai.com/api/v0/pipeline";

    $response = make_request($api_url, $headers, $body);

    if ($response['http_code'] !== 200) {
        return [null, $response['response']];
    }

    $data = json_decode($response['response'], true);
    $labels = $data['output'][0]['labels'] ?? [];
    $headline = end($labels)['value'] ?? null;
    return [$headline, null];
}

function get_embedding_body($headline) {
    return json_encode([
        "input" => $headline,
        "input_type" => "article",
        "labels" => [["skill" => "embedding", "value" => null, "span_text" => $headline]],
        "steps" => [
            [
                "skill" => "embedding",
                "params" => [
                    "return_embeddings" => true,
                    "domain" => "intent@1",
                ],
            ],
        ],
    ]);
}

function get_embeddings($headline) {
    $headers = getHeaders();
    $body = get_embedding_body($headline);
    $api_url = "https://api.oneai.com/api/v0/pipeline";

    $response = make_request($api_url, $headers, $body);

    if ($response['http_code'] !== 200) {
        return [null, $response['response']];
    }

    $data = json_decode($response['response'], true);
    $labels = $data['output'][0]['labels'] ?? [];
    $embeddings = end($labels)['value'] ?? null;
    return [$embeddings, null];
}

function make_request($api_url, $headers, $body) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return ['response' => $response, 'http_code' => $http_code];
}

function performVectorSearch($embedding, $indexName, $numCandidates = 5000, $limit = 1) {
    try {
        $client = new Client(CONNECTION_STRING);
        $collection = $client->selectCollection(DATABASE_NAME, COLLECTION_NAME);

        $pipeline = [
            [
                '$vectorSearch' => [
                    'index' => $indexName,
                    'path' => 'embedding_vector',
                    'queryVector' => $embedding,
                    'numCandidates' => $numCandidates,
                    'limit' => $limit,
                ]
            ]
        ];

        $cursor = $collection->aggregate($pipeline);

        foreach ($cursor as $document) {
            return $document['topic'];
        }

        return null;
    } catch (Exception $e) {
        echo "An error occurred: " . $e->getMessage();
        return null;
    }
}

function getHeadlineAndEmbedding($website) {
    list($headline, $headlineError) = get_headline($website);
    if ($headlineError) {
        echo "Error getting headline: " . $headlineError;
        return null;
    }

    list($embedding, $embeddingError) = get_embeddings($headline);
    if ($embeddingError) {
        echo "Error getting embedding: " . $embeddingError;
        return null;
    }

    return  $embedding;
}


$website = "https://edition.cnn.com/2024/06/22/uk/taylor-swift-royals-eras-tour-london-intl/index.html?iid=cnn_buildContentRecirc_end_recirc";
$embedding_vector = getHeadlineAndEmbedding($website);

if ($embedding_vector) {
    $topic = performVectorSearch($embedding_vector, 'vector_index', 5000, 1); // Set a very high numCandidates
    if ($topic) {
        echo "Closest topic: " . $topic;
    } else {
        echo "No matching topics found.";
    }
} else {
    echo "Failed to generate headline or embedding.";
}
?>