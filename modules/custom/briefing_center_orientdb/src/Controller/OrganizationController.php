<?php

namespace Drupal\briefing_center_orientdb\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines OrganizationController class.
 */
class OrganizationController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content($rid) {
    return [
      '#type' => 'markup',
      '#markup' => '<div id="orientdb-organization" rid="' . $rid . '"></div>',
      '#attached' => [
        'library' => ['briefing_center_orientdb/orientdb-organization'],
      ]
    ];
  }

  /**
   * Generate a dynamic title for the route.
   *
   * @return array
   *   Return markup array.
   */
  public function getTitle($rid) {
    return 'Organization: ' . $rid;
  }

}