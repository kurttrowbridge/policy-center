<?php

namespace Drupal\briefing_center_orientdb\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines EventController class.
 */
class EventController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content($rid) {
    return [
      '#type' => 'markup',
      '#markup' => '<div id="orientdb-event" rid="' . $rid . '"></div>',
      '#attached' => [
        'library' => ['briefing_center_orientdb/orientdb-event-list'],
      ]
    ];
  }

  /**
   * Generate a dynamic title for the route.
   *
   * @return array
   *   Return markup array.
   */
  public function getTitle($rid) {
    return 'Event: ' . $rid;
  }

}