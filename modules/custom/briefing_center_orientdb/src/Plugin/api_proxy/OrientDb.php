<?php

namespace Drupal\briefing_center_orientdb\Plugin\api_proxy;

use Drupal\api_proxy\Plugin\api_proxy\HttpApiCommonConfigs;
use Drupal\api_proxy\Plugin\HttpApiPluginBase;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Form\SubformStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * The Example API.
 *
 * @HttpApi(
 *   id = "orient-db-api",
 *   label = @Translation("OrientDB API"),
 *   description = @Translation("Proxies requests to the OrientDB API."),
 *   serviceUrl = "52.55.8.64:2480/query/policycenter/sql/",
 * )
 */
final class OrientDb extends HttpApiPluginBase {

  use HttpApiCommonConfigs;

  /**
   * The configuration service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): HttpApiPluginBase {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->config = $container->get('config.factory')->get('api_proxy.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl(): string {
    return '52.55.8.64:2480/query/policycenter/sql/';
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreConfigurationFormElements(array $form, SubformStateInterface $form_state): array {
    // $form['auth_token'] = $this->authTokenConfigForm($this->configuration);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function calculateHeaders(array $headers): array {
    $new_headers = parent::calculateHeaders($headers);
    // Modify & add new headers. Here you can add the auth token.
    $username = $this->config->get('api_proxies.orient-db-api.username');
    $password = $this->config->get('api_proxies.orient-db-api.password');
    $auth = base64_encode($username . ":" . $password);
    $new_headers['authorization'] = "Basic $auth";
    return $new_headers;
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessOutgoing(Response $response): Response {
    // Modify the response from the API.
    // A common problem is to remove the Transfer-Encoding header.
    // $response->headers->remove('transfer-encoding');
    return $response;
  }

}