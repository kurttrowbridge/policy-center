<?php

namespace Drupal\briefing_center_orientdb\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for OrientDB Glossary data.
 *
 *  @Block(
 *      id = "briefing_center_orientdb_glossary",
 *      admin_label = @Translation("OrientDB Glossary block"),
 *      category = @Translation("Briefing Center"),
 *  )
 */
class OrientDbGlossary extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'briefing_center_orientdb.orientdb.glossary',
    ];
  }

}
