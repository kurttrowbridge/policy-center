<?php

namespace Drupal\briefing_center_orientdb\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for OrientDB Person page data.
 *
 *  @Block(
 *      id = "briefing_center_orientdb_person",
 *      admin_label = @Translation("OrientDB Person block"),
 *      category = @Translation("Briefing Center"),
 *  )
 */
class OrientDbPerson extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'briefing_center_orientdb.orientdb.person',
    ];
  }

}
