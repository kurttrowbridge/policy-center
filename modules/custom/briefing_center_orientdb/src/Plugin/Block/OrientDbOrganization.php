<?php

namespace Drupal\briefing_center_orientdb\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for OrientDB Organization page data.
 *
 *  @Block(
 *      id = "briefing_center_orientdb_organization",
 *      admin_label = @Translation("OrientDB Organization block"),
 *      category = @Translation("Briefing Center"),
 *  )
 */
class OrientDbOrganization extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'briefing_center_orientdb.orientdb.organization',
    ];
  }

}
