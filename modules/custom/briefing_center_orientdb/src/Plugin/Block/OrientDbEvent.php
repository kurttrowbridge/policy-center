<?php

namespace Drupal\briefing_center_orientdb\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for OrientDB Event page data.
 *
 *  @Block(
 *      id = "briefing_center_orientdb_event",
 *      admin_label = @Translation("OrientDB Event block"),
 *      category = @Translation("Briefing Center"),
 *  )
 */
class OrientDbEvent extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'briefing_center_orientdb.orientdb.event',
    ];
  }

}
