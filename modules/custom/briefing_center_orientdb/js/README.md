To make and test updates locally, run `npm run watch` from the `js` directory.
To make updates for production, run `npm run build` from the `js` directory.
