import React from 'react';
import ReactDOM from 'react-dom';
import Organization from './components/Organization';

const organizationElement = document.getElementById('orientdb-organization');
if (organizationElement) {
  const organizationRid = organizationElement.getAttribute('rid');

  ReactDOM.render(
    <React.StrictMode>
      <Organization rid={organizationRid} />
    </React.StrictMode>,
    organizationElement
  );
}