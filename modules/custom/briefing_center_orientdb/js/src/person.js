import React from 'react';
import ReactDOM from 'react-dom';
import Person from './components/Person';

const personElement = document.getElementById('orientdb-person');
if (personElement) {
  const personRid = personElement.getAttribute('rid');

  ReactDOM.render(
    <React.StrictMode>
      <Person rid={personRid} />
    </React.StrictMode>,
    personElement
  );
}