import React from 'react';
import ReactDOM from 'react-dom';
import EventList from './components/EventList';
import Event from './components/Event';
import Glossary from './components/Glossary';

const element = document.getElementById('orientdb-event-list');
if (element) {
  const rid = element.getAttribute('rid');

  ReactDOM.render(
    <React.StrictMode>
      <EventList rid={rid} />
    </React.StrictMode>,
    element
  );
}

const eventElement = document.getElementById('orientdb-event');
if (eventElement) {
  const eventRid = eventElement.getAttribute('rid');

  ReactDOM.render(
    <React.StrictMode>
      <Event rid={eventRid} />
    </React.StrictMode>,
    eventElement
  );
}

const glossaryElement = document.getElementById('orientdb-glossary');
if (glossaryElement) {
  const glossaryRid = glossaryElement.getAttribute('rid');

  ReactDOM.render(
    <React.StrictMode>
      <Glossary rid={glossaryRid} />
    </React.StrictMode>,
    glossaryElement
  );
}