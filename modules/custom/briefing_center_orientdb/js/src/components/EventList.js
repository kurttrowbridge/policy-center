import React, { useState, useEffect } from "react";
import Events from "./Events";

export default function EventList(props) {
  const [appState, setAppState] = useState({
    loaded: false,
    events: [],
  });

  const [isReversed, setIsReversed] = useState(false);

  // Get data out of OrientDB.
  const fetchData = () => {
    let url =
      "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
      props.rid.replace("-", ":") +
      " maxdepth 2) order by Date";

    let headers = new Headers();

    headers.append("Authorization", "None");

    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        const events = json.result[0]["in_Event_issue"];
        const allEvents = [];
        events.map((event_issue) => {
          let event_issue_no_hash = event_issue.replace("#", "");

          let event_issue_url =
            "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
            event_issue_no_hash +
            " maxdepth 2) order by Date";

          let headers = new Headers();

          headers.append("Authorization", "None");

          fetch(event_issue_url, {
            method: "GET",
            headers: headers,
          })
            .then((response) => response.json())
            .then((json) => {
              let issue_number = json.result[0]["out"].replace("#", "");

              let issue_url =
                "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
                issue_number +
                " maxdepth 2) order by Date";

              let headers = new Headers();

              headers.append("Authorization", "None");

              fetch(issue_url, {
                method: "GET",
                headers: headers,
              })
                .then((response) => response.json())
                .then((json) => {
                  function sort_by_date(a, b) {
                    return (
                      new window.Date(a.Date.replace(/-/g, "/")).getTime() -
                      new window.Date(b.Date.replace(/-/g, "/")).getTime()
                    );
                  }

                  const events = json.result.filter((item) => {
                    return item["@class"] == "Events";
                  });

                  allEvents.push(events[0]);

                  // Start in reverse-chron order, then reverse again when rendering so it's toggleable.
                  allEvents.sort(sort_by_date).reverse();

                  setAppState({
                    ...appState,
                    loaded: true,
                    events: allEvents,
                  });
                });
            });
        });
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {appState.loaded === true && appState.events.length > 0 ? (
        <>
          <div className="button-group">
            <button
              className="button-outline"
              onClick={() => setIsReversed(!isReversed)}
            >
              Reverse order
            </button>
          </div>
          <Events events={appState.events.reverse()} />
        </>
      ) : (
        <p>No timeline events found.</p>
      )}
    </>
  );
}
