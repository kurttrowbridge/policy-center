import React from 'react';

const News = (props) => {
    const { news } = props;
    return (
        <>
            {news.map((news, index) => {
                return (
                    <>
                        <article className="node story">
                            <h3 className="page-title story__heading">
                                <a href={news['Source-url']}>{news['Hed']}</a>
                            </h3>
                            <span className="story__category">{news['Slug-heading']}</span>
                            <span className="story__sub-category">
                                <strong>{news['Source-name']}</strong>
                            </span>
                        </article>
                    </>
                )
            })}
        </>
    )
}

export default News;