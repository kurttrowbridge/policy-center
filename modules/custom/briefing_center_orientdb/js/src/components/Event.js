import React, { useState, useEffect } from "react";
import moment from "moment";
import Documents from "./Documents";
import Organizations from "./Organizations";
import Persons from "./Persons";
import News from "./News";
import RelatedTopic from "./RelatedTopic";

const Event = (props) => {
  const [appState, setAppState] = useState({
    loaded: false,
    event: [],
    eventTitle: null,
    eventDate: null,
    body: null,
    documents: [],
    organizations: [],
    persons: [],
    news: [],
  });

  const [topicState, setTopicState] = useState({
    loaded: false,
    topics: [],
  });

  const [activeTab, setActiveTab] = useState('news');

  // Get data out of OrientDB.
  const fetchData = () => {
    let url =
      "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
      props.rid.replace("-", ":") +
      " maxdepth 2) order by Date";

    let headers = new Headers();

    headers.append(
      "Authorization",
      "None"
    );

    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        const event = json.result.filter((item) => {
          return item["@class"] == "Events";
        });
        const action = event[0]["Action"];
        const shortName = event[0]["Short_name"];
        const eventTitle = shortName ? shortName : action;
        document.title = eventTitle + " | briefing·center";
        const eventDate = event[0]["Date"];
        const topics = json.result.filter((item) => {
          return item["@class"] == "Topics";
        });

        const documents = json.result.filter((item) => {
          return item["@class"] == "Documents";
        });

        const organizations = json.result.filter((item) => {
          return item["@class"] == "Entities";
        });

        const persons = json.result.filter((item) => {
          return item["@class"] == "Persons";
        });

        const news = json.result.filter((item) => {
          return item["@class"] == "News";
        });

        setAppState({
          ...appState,
          loaded: true,
          event: event,
          eventTitle: eventTitle,
          eventDate: eventDate,
          body: shortName ? action : null,
          documents: documents,
          organizations: organizations,
          persons: persons,
          news: news,
        });

        let topicItems = [];
        topics.map((topic) => {
          let topicId = topic['@rid'];
          if (topicId) {
            let url = "/jsonapi/node/topic?filter[field__rid]=" + topicId.replace("#", "");
  
            fetch(url, {
              method: "GET",
            })
              .then((response) => response.json())
              .then((json) => {
                const topicAttributes = json.data[0]["attributes"];
                const topicTitle = topicAttributes["title"];
                const topicPath = topicAttributes["path"]["alias"];
                const topicSOP =
                  topicAttributes["field_state_of_play"]["processed"];
                topicItems.push({
                  title: topicTitle,
                  path: topicPath,
                  stateofPlay: topicSOP,
                });
                setTopicState({
                  ...topicState,
                  loaded: true,
                  topics: topicItems,
                });
              });
          }
        })
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="content-wrapper no-margin">
      <div className="page-content">
        <div className="page-type ts-h2">Event</div>
        {appState.loaded === true ? (
          <>
            <section className="section">
              <h1 className="page-title page-title--no-margin">
                {appState.eventTitle}
              </h1>
              <strong>
                {moment(appState.eventDate).format("MMMM D, YYYY")}
              </strong>
              {appState.body ? <p>{appState.body}</p> : undefined}
              <div>
                <ul className="topic-tabs__group">
                  {appState.loaded === true && appState.news.length ? (
                    <li className={`topic-tab ${activeTab == 'news' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="news"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >News</button>
                    </li>
                  ) : undefined}
                  {appState.loaded === true && appState.documents.length ? (
                    <li className={`topic-tab ${activeTab == 'documents' ? 'current' : ''}`} role="presentation">
                        <button
                          data-value="documents"
                          onClick={(e) => {
                            setActiveTab(e.target.dataset.value);
                          }}
                        >
                          Documents
                        </button>
                    </li>
                  ) : undefined}
                  {appState.loaded === true && appState.organizations.length ? (
                    <li className={`topic-tab ${activeTab == 'organizations' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="organizations"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >Organizations</button>
                    </li>
                  ) : undefined}
                  {appState.loaded === true && appState.persons.length ? (
                    <li className={`topic-tab ${activeTab == 'persons' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="persons"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >Persons</button>
                    </li>
                  ) : undefined}
                </ul>
                {appState.loaded === true && appState.news.length && activeTab == 'news' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--news">
                    <h2 className="visually-hidden" id="topic-panel__heading--news">News</h2>
                    <News news={appState.news} />
                  </div>
                ) : undefined}
                {appState.loaded === true && appState.documents.length && activeTab == 'documents' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--documents">
                    <h2 className="visually-hidden" id="topic-panel__heading--documents">Documents</h2>
                    <Documents documents={appState.documents} />
                  </div>
                ) : undefined}
                {appState.loaded === true && appState.organizations.length && activeTab == 'organizations' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--organizations">
                    <h2 className="visually-hidden" id="topic-panel__heading--organizations">Organizations</h2>
                    <Organizations organizations={appState.organizations} />
                  </div>
                ) : undefined}
                {appState.loaded === true && appState.persons.length && activeTab == 'persons' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--persons">
                    <h2 className="visually-hidden" id="topic-panel__heading--persons">Persons</h2>
                    <Persons persons={appState.persons} />
                  </div>
                ) : undefined}
              </div>
            </section>
            {topicState.loaded == true && topicState.topics.map((topic) => (
              <RelatedTopic key={topic.path} path={topic.path} title={topic.title} stateofPlay={topic.stateofPlay} />
            ))}
          </>
        ) : undefined}
      </div>
      <div id="sidebar-first" className="column sidebar"></div>
    </div>
  );
};

export default Event;
