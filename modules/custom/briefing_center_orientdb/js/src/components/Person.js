import React, { useState, useEffect } from "react";
import { decode as base64_decode, encode as base64_encode } from "base-64";
import Events from "./Events";
import NewsDrupal from "./NewsDrupal";

const Person = (props) => {
  const [appState, setAppState] = useState({
    loaded: false,
    person: [],
    personName: null,
    jobTitle: null,
    description: null,
    employer: null,
    employerRid: null,
  });

  const [newsState, setNewsState] = useState({
    loaded: false,
    news: [],
  });

  const [eventsState, setEventsState] = useState({
    loaded: false,
    events: [],
  });

  const [activeTab, setActiveTab] = useState('events');

  // Get data out of OrientDB.
  const fetchData = () => {
    let url =
      "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
      props.rid.replace("-", ":") +
      " maxdepth 2) order by Date";

    let headers = new Headers();

    headers.append(
      "Authorization",
      "None"
    );

    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        const person = json.result.filter((item) => {
          return item["@class"] == "Persons";
        });
        const firstName = person[0] ? person[0]["Name_first"] : null;
        const lastName = person[0] ? person[0]["Name_last"] : null;
        const personName = firstName + ' ' + lastName;
        document.title = personName + " | briefing·center";
        const description = person[0] ? person[0]["Description"] : null;

        const employedBy = json.result.filter((item) => {
          return item["@class"] == "Employed_by";
        });
        const organization = json.result.filter((item) => {
          return item["@class"] == "Organizations";
        });

        const events = [];

        const participatedInEvents = json.result.filter((item) => {
          return item["@class"] == "Participated_in";
        });

        const jobTitle = employedBy[0]['job_title'];
        const employer = organization[0] ? organization[0]['Name'] : null;
        const employerRid = organization[0] ? organization[0]['@rid'].replace('#', '').replace(':', '-') : null;

        let news_url = "/jsonapi/index/content_index?filter[fulltext]=" + personName + "&filter[type]=news&sort=-nid";
        fetch(news_url).then((newsResponse) => newsResponse.json())
          .then((json) => {
            const news = json.data;
            setNewsState({
              ...newsState,
              loaded: true,
              news: news,
            });
          });

        if (participatedInEvents.length > 0) {
          participatedInEvents.map((event) => {
            const eventRid = event['in'];

            let event_url =
            "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " + eventRid.replace('#', '') + " maxdepth 2) order by Date";

            let headers = new Headers();
        
            headers.append(
              "Authorization",
              "None"
            );

            fetch(event_url, {
              method: "GET",
              headers: headers,
            })
            .then(response => response.json())
            .then(json => {
              function sort_by_date(a,b) {
                return new window.Date(a.Date).getTime() - new window.Date(b.Date).getTime();
              }

              const orgEvents = json.result.filter(item => {
                return item['@class'] == 'Events';
              });

              const rid = orgEvents[0] ? orgEvents[0]['@rid'] : null;
              const Date = orgEvents[0] ? orgEvents[0]['Date'] : null;
              const Action = orgEvents[0] ? orgEvents[0]['Action'] : null;

              const event = [{
                '@rid': rid,
                'Date': Date,
                'Action': Action
              }];

              const newEvent = events.push(event[0]);

              events.sort(sort_by_date);

              setEventsState({
                ...eventsState,
                loaded: true,
                events: events,
              });
            });
          })
        }
        setAppState({
          ...appState,
          loaded: true,
          person: person,
          personName: personName,
          jobTitle: jobTitle,
          description: description,
          employer: employer,
          employerRid: employerRid,
        });
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="content-wrapper no-margin">
      <div className="page-content">
        <div className="page-type ts-h2">Person</div>
        {appState.loaded === true ? (
          <>
            <section className="section">
              <h1 className="page-title page-title--no-margin">
                {appState.personName}
              </h1>
              <strong>
                {appState.jobTitle}{appState.employer ? (<>, <a href={'/organizations/' + appState.employerRid}>{appState.employer}</a></>) : undefined}
              </strong>
              {appState.description ? <p>{appState.description}</p> : undefined}
              <div>
                <ul className="topic-tabs__group">
                  {eventsState.loaded === true && eventsState.events.length ? (
                    <li className={`topic-tab ${activeTab == 'events' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="events"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >Events</button>
                    </li>
                  ) : undefined}
                  {newsState.loaded === true && newsState.news.length ? (
                    <li className={`topic-tab ${activeTab == 'news' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="news"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >News</button>
                    </li>
                  ) : undefined}
                </ul>
                {eventsState.loaded === true && eventsState.events.length && activeTab == 'events' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--events">
                    <h2 className="visually-hidden" id="topic-panel__heading--events">Events</h2>
                    <Events events={eventsState.events} />
                  </div>
                ) : undefined}
                {newsState.loaded === true && newsState.news.length && activeTab == 'news' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--news">
                    <h2 className="visually-hidden" id="topic-panel__heading--news">News</h2>
                    <NewsDrupal news={newsState.news} />
                  </div>
                ) : undefined}
              </div>
            </section>
          </>
        ) : undefined}
      </div>
      <div id="sidebar-first" className="column sidebar"></div>
    </div>
  );
};

export default Person;
