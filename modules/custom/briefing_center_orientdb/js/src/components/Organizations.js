import React from 'react';
// import moment from 'moment';

const Organizations = (props) => {
    const { organizations } = props;
    return (
        <>
            {organizations.map((organization, index) => {
                return (
                    <>
                        <div className='timeline-item' key={organization['@rid']}>
                            {/* <div className='timeline__date'><strong>{moment(organization.Date).format('MMMM D, YYYY')}</strong></div> */}
                            <div className='timeline__description'>
                                <a href={'/organizations/' + organization['@rid'].replace('#', '').replace(':', '-')}>
                                    <strong>{organization.Name}</strong>
                                </a>
                                <br />
                                <span dangerouslySetInnerHTML={{__html: organization.Location}}></span>
                            </div>
                        </div>
                    </>
                )
            })}
        </>
    )
}

export default Organizations;