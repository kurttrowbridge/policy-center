import React from 'react';
import moment from 'moment';

const Documents = (props) => {
    const { documents } = props;
    return (
        <>
            {documents.map((document, index) => {
                return (
                    <>
                        <div className='timeline-item' key={document['@rid']}>
                            <div className='timeline__date'><strong>{moment(document.Date).format('MMMM D, YYYY')}</strong></div>
                            <div className='timeline__description'>
                                <a href={document['Link'].replace('#', '')} target="_blank">{document.Title}</a>
                                <br />
                                <span dangerouslySetInnerHTML={{__html: document.Summary}}></span>
                            </div>
                        </div>
                    </>
                )
            })}
        </>
    )
}

export default Documents;