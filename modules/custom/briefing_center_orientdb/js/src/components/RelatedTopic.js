import React from "react";

const RelatedTopic = (props) => {
  return (
    <div className="content__topics">
      <div className="news__topics">
        <div className="topic topic--lg">
          <h2 className="topic__heading">
            <span className="topic__label">Related Topic: </span>
            <a href={props.path} tabIndex="-1">
              <span>{props.title}</span>
            </a>
          </h2>
          <div className="topic__details">
            <div>
              <strong>State of Play</strong>
            </div>
            <div
              dangerouslySetInnerHTML={{
                __html: props.stateofPlay,
              }}
            ></div>
            <strong>
              <a href={props.path}>
                More information
                <span className="visually-hidden">
                  about <span>{props.title}</span>
                </span>
              </a>
            </strong>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RelatedTopic;
