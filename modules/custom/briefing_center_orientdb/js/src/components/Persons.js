import React from 'react';
// import moment from 'moment';

const Persons = (props) => {
    const { persons } = props;
    return (
        <>
            {persons.map((person, index) => {
                return (
                    <div key={person[0] && person[0]['personRid'] ? person[0]['personRid'] : person['@rid']}>
                        <div className='timeline-item' key={person[0] && person[0]['personRid'] ? person[0]['personRid'] : person['@rid']}>
                            {/* <div className='timeline__date'><strong>{moment(person.Date).format('MMMM D, YYYY')}</strong></div> */}
                            <div className='timeline__description'>
                                <a href={'/persons/' + (person[0] && person[0]['personRid'] ? person[0]['personRid'].replace('#', '').replace(':', '-') : person['@rid'].replace('#', '').replace(':', '-'))}>
                                    <strong>{person[0] && person[0]['firstName'] ? person[0]['firstName'] : person['Name_first']} {person[0] && person[0]['lastName'] ? person[0]['lastName'] : person['Name_last']}</strong>
                                </a>
                                <br />
                                <span dangerouslySetInnerHTML={{__html: person[0] && person[0]['jobTitle'] ? person[0]['jobTitle'] : person['Title']}}></span>
                            </div>
                        </div>
                    </div>
                )
            })}
        </>
    )
}

export default Persons;