import React from 'react';

const NewsDrupal = (props) => {
    const { news } = props;
    return (
        <>
            {news.map((news) => {
                return (
                    <>
                        <article className="node story">
                            <h3 className="page-title story__heading">
                                <a href={news.attributes.path.alias}>{news.attributes.title}</a>
                            </h3>
                            <span className="story__category">News</span>
                            <span className="story__sub-category">
                                <strong>{news.attributes.field_author}</strong>
                            </span>
                        </article>
                    </>
                )
            })}
        </>
    )
}

export default NewsDrupal;