import React, { useState, useEffect } from "react";
import { decode as base64_decode, encode as base64_encode } from "base-64";
import Events from "./Events";
import Persons from "./Persons";
import NewsDrupal from "./NewsDrupal";

const Organization = (props) => {
  const [appState, setAppState] = useState({
    loaded: false,
    organization: [],
    organizationTitle: null,
    organizationName: null,
    city: null,
    type: null,
    description: null,
    events: [],
    persons: [],
  });

  const [activeTab, setActiveTab] = useState('events');

  const [newsState, setNewsState] = useState({
    loaded: false,
    news: [],
  });

  // Get data out of OrientDB.
  const fetchData = () => {
    let url =
      "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
      props.rid.replace("-", ":") +
      " maxdepth 2) order by Date";

    let headers = new Headers();

    headers.append(
      "Authorization",
      "None"
    );

    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        const organization = json.result.filter((item) => {
          return item["@class"] == "Organizations";
        });
        const city = organization[0] ? organization[0]["Location_city"] : null;
        const name = organization[0] ? organization[0]["Name"] : null;
        const type = organization[0] ? organization[0]["Type"] : null;
        const organizationName = name;
        document.title = organizationName + " | briefing·center";
        const description = organization[0] ? organization[0]["Description"] : null;

        let news_url = "/jsonapi/index/content_index?filter[fulltext]=" + organizationName + "&filter[type]=news&sort=-nid";
        fetch(news_url).then((newsResponse) => newsResponse.json())
          .then((json) => {
            const news = json.data;
            setNewsState({
              ...newsState,
              loaded: true,
              news: news,
            });
          });

        const initialEvents = json.result.filter((item) => {
          return item["@class"] == ("Initiated" || "Recipient_of");
        });

        const events = [];

        const initialPersons = json.result.filter((item) => {
          return item["@class"] == "Persons";
        });

        const persons = [];

        initialPersons.map((person) => {
          const employedByRid = person['out_Employed_by'][0];
          const firstName = person['Name_first'];
          const lastName = person['Name_last'];

          let employed_by_url =
          "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " + employedByRid.replace('#', '') + " maxdepth 2) order by Date";
      
          let headers = new Headers();
      
          headers.append(
            "Authorization",
            "None"
          );

          fetch(employed_by_url, {
            method: "GET",
            headers: headers,
          })
          .then(response => response.json())
          .then(json => {
            const jobTitle = json.result[0]['job_title'];
            const personRid = json.result[0]['out'];

            const person = [{
              firstName,
              lastName,
              personRid,
              jobTitle
            }];

            const newPerson = persons.push(person);

            setAppState({
              ...appState,
              loaded: true,
              organization: organization,
              organizationName: organizationName,
              city: city,
              type: type,
              description: description,
              events: events,
              persons: persons,
            });
          });
        })

        initialEvents.map((event) => {
          const eventRid = event['in'];

          let event_url =
          "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " + eventRid.replace('#', '') + " maxdepth 2) order by Date";
      
          let headers = new Headers();
      
          headers.append(
            "Authorization",
            "None"
          );

          fetch(event_url, {
            method: "GET",
            headers: headers,
          })
          .then(response => response.json())
          .then(json => {
            function sort_by_date(a,b) {
              return new window.Date(a.Date).getTime() - new window.Date(b.Date).getTime();
            }

            const orgEvents = json.result.filter(item => {
              return item['@class'] == 'Events';
            });

            const rid = orgEvents[0] ? orgEvents[0]['@rid'] : null;
            const Date = orgEvents[0] ? orgEvents[0]['Date'] : null;
            const Action = orgEvents[0] ? orgEvents[0]['Action'] : null;

            const event = [{
              '@rid': rid,
              'Date': Date,
              'Action': Action
            }];

            const newEvent = events.push(event[0]);

            events.sort(sort_by_date);

            setAppState({
              ...appState,
              loaded: true,
              organization: organization,
              organizationName: organizationName,
              city: city,
              type: type,
              description: description,
              events: events,
              persons: persons,
            });
          });
        })
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="content-wrapper no-margin">
      <div className="page-content">
        <div className="page-type ts-h2">Organization</div>
        {appState.loaded === true ? (
          <>
            <section className="section">
              <h1 className="page-title page-title--no-margin">
                {appState.organizationName}
              </h1>
              <strong>
                {appState.type} &middot; {appState.city}
              </strong>
              {appState.description ? <p>{appState.description}</p> : undefined}
              <div>
                <ul className="topic-tabs__group">
                  {appState.loaded === true && appState.events.length ? (
                    <li className={`topic-tab ${activeTab == 'events' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="events"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >Events</button>
                    </li>
                  ) : undefined}
                  {newsState.loaded === true && newsState.news.length ? (
                    <li className={`topic-tab ${activeTab == 'news' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="news"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >News</button>
                    </li>
                  ) : undefined}
                  {appState.loaded === true && appState.persons.length ? (
                    <li className={`topic-tab ${activeTab == 'persons' ? 'current' : ''}`} role="presentation">
                      <button
                        data-value="persons"
                        onClick={(e) => {
                          setActiveTab(e.target.dataset.value);
                        }}
                      >Persons</button>
                    </li>
                  ) : undefined}
                </ul>
                {appState.loaded === true && appState.events.length && activeTab == 'events' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--events">
                    <h2 className="visually-hidden" id="topic-panel__heading--events">Events</h2>
                    <Events events={appState.events} />
                  </div>
                ) : undefined}
                {newsState.loaded === true && newsState.news.length && activeTab == 'news' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--news">
                    <h2 className="visually-hidden" id="topic-panel__heading--news">News</h2>
                    <NewsDrupal news={newsState.news} />
                  </div>
                ) : undefined}
                {appState.loaded === true && appState.persons.length && activeTab == 'persons' ? (
                  <div className="tabPanel" aria-hidden="false" aria-labelledby="topic-panel__heading--persons">
                    <h2 className="visually-hidden" id="topic-panel__heading--persons">Persons</h2>
                    <Persons persons={appState.persons} />
                  </div>
                ) : undefined}
              </div>
            </section>
          </>
        ) : undefined}
      </div>
      <div id="sidebar-first" className="column sidebar"></div>
    </div>
  );
};

export default Organization;
