import React from 'react';
import moment from 'moment';

const Events = (props) => {
    const { events } = props;
    return (
        <>
            {events.map((event, index) => {
                if (event === undefined) {
                    return null;
                }

                return (
                    <span key={event['@rid']}>
                        <div className='timeline-item' key={event['@rid']}>
                            <div className='timeline__date'><strong>{moment(event.Date).format('MMMM D, YYYY')}</strong></div>
                            <div className='timeline__description'><a href={'/events/' + event['@rid'].replace('#', '').replace(':', '-')} className="no-underline">{event.Action}</a></div>
                        </div>
                    </span>
                )
            })}
        </>
    )
}

export default Events;