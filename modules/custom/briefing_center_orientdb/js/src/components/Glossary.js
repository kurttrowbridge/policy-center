import React, { useState, useEffect } from "react";
import Terms from "./Terms";

export default function Glossary(props) {
  const [appState, setAppState] = useState({
    loaded: false,
    terms: [],
  });

  // Get data out of OrientDB.
  const fetchData = () => {
    let url =
      "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
      props.rid.replace("-", ":") +
      " maxdepth 3) order by Date";

    let headers = new Headers();

    headers.append("Authorization", "None");

    fetch(url, {
      method: "GET",
      headers: headers,
    })
      .then((response) => response.json())
      .then((json) => {
        const termRids = json.result[0]["in_related_term"];

        const termList = [];

        if (termRids && termRids.length > 0) {
          termRids.map((rid) => {
            let interUrl =
              "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
              rid.replace("#", "") +
              " maxdepth 3)";

            let headers = new Headers();

            headers.append("Authorization", "None");

            fetch(interUrl, {
              method: "GET",
              headers: headers,
            })
              .then((response) => response.json())
              .then((json) => {
                const relatedTerm = json.result.filter((item) => {
                  return item["@class"] == "related_term";
                });

                const outRid = relatedTerm[0]["out"];

                let termUrl =
                  "/api-proxy/orient-db-api?_api_proxy_uri=select * from (traverse * from " +
                  outRid.replace("#", "") +
                  " maxdepth 3)";

                fetch(termUrl, {
                  method: "GET",
                  headers: headers,
                })
                  .then((response) => response.json())
                  .then((json) => {
                    const terms = json.result.filter((item) => {
                      return item["@class"] == "Terms";
                    });

                    const name = terms[0]["Term"];
                    const definition = terms[0]["Definition"];

                    const term = [
                      {
                        name,
                        definition,
                      },
                    ];

                    const newTerm = termList.push(term[0]);

                    // Order terms alphabetically.
                    termList.sort((a, b) => a.name.localeCompare(b.name));

                    // Remove duplicate terms.
                    let uniqueTerms = [
                      ...new Map(termList.map((m) => [m.name, m])).values(),
                    ];

                    setAppState({
                      ...appState,
                      loaded: true,
                      terms: uniqueTerms,
                    });
                  });
              });
          });
        }
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      {appState.loaded === true && appState.terms.length > 0 ? (
        <Terms terms={appState.terms} />
      ) : (
        <p>No glossary definitions found.</p>
      )}
    </>
  );
}
