import React from "react";

const Terms = (props) => {
  const { terms } = props;
  return (
    <>
      {terms.map((term, index) => (
        <div className="timeline-item" key={index}>
          <div className="timeline__date">
            <strong>{term.name}</strong>
          </div>
          <div className="timeline__description">{term.definition}</div>
        </div>
      ))}
    </>
  );
};

export default Terms;
