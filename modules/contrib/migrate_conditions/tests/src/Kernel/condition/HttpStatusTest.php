<?php

namespace Drupal\Tests\migrate_conditions\Kernel\Plugin\condition;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the http_status condition plugin.
 *
 * @group migrate_conditions
 */
class HttpStatusTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'migrate_conditions',
  ];

  /**
   * Tests with a real guzzle service.
   *
   * @dataProvider providerTestEvaluate
   */
  public function testEvaluate($url, $configuration, $expected) {
    $row = $this->createMock('Drupal\migrate\Row');
    $condition = \Drupal::service('plugin.manager.migrate_conditions.condition')->createInstance('http_status', $configuration);
    $this->assertSame($expected, $condition->evaluate($url, $row));
  }

  /**
   * Provides test cases for testEvaluate().
   */
  public static function providerTestEvaluate() {
    return [
      'found' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions',
        'configuration' => [],
        'expected' => TRUE,
      ],
      'found again' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions',
        'configuration' => ['code' => 200],
        'expected' => TRUE,
      ],
      'found again again' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions',
        'configuration' => ['code' => 500],
        'expected' => FALSE,
      ],
      'not found' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions_test',
        'configuration' => [],
        'expected' => FALSE,
      ],
      'not found again' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions_test',
        'configuration' => ['code' => 403],
        'expected' => FALSE,
      ],
      'not found again again' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions_test',
        'configuration' => ['code' => 404],
        'expected' => TRUE,
      ],
      'not found again again again' => [
        'url' => 'https://www.drupal.org/project/migrate_conditions_test',
        'configuration' => ['code' => [403, '404']],
        'expected' => TRUE,
      ],
      'some crazy non-url' => [
        'url' => 'Hey, Tex!',
        'configuration' => [],
        'expected' => FALSE,
      ],
      'some crazy non-url again' => [
        'url' => 'Hey, Tex!',
        'configuration' => ['code' => 404],
        'expected' => FALSE,
      ],
    ];
  }

}
