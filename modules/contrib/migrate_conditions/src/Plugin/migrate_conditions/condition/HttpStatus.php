<?php

namespace Drupal\migrate_conditions\Plugin\migrate_conditions\condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Row;
use Drupal\migrate_conditions\Plugin\ConditionBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'http_status' condition.
 *
 * This performs a HEAD request to the source url and compares the http
 * response status code (200, 404, etc.) to the configured code.
 *
 * Available configuration keys:
 * - code: (optional) The response status code(s) we are comparing to. If
 *   not specified, the condition will return false for any 4xx or 5xx
 *   response, which should cover most use cases.
 * - negate: (optional) Whether the 'http_status' condition should be
 *   negated. Defaults to FALSE. You can also negate the 'http_status'
 *   plugin by using 'not:http_status' as the plugin id.
 * - source: (optional) Property or array of properties on which to evaluate
 *   the condition. If not set, the condition will be evaluated on the source
 *   passed to the ::evaluate() method, typically the source of the process
 *   plugin that is using this condition.
 *
 * Examples:
 *
 * 1. Skip the row if a file url gives 4xx or 5xx response.
 *
 * @code
 * process:
 *   _file_trouble:
 *     plugin: skip_on_condition
 *     method: row
 *     source: my_source_url
 *     condition: not:http_status
 * @endcode
 *
 * or simply skip processing a file field before download fails:
 *
 * @code
 * process:
 *   path_to_file:
 *     -
 *       plugin: skip_on_condition
 *       method: process
 *       source: source_url
 *       condition: not:http_status
 *     -
 *       plugin: download
 *       source:
 *         - source_url
 *         - destination_uri
 * @endcode
 *
 * 2. Store the teapot status.
 *
 * @code
 * process:
 *   field_is_a_teapot:
 *     plugin: evaluate_condition
 *     source: my_source_url
 *     condition:
 *       plugin: http_status
 *       code: 418
 * @endcode
 *
 * or equivalently
 *
 * @code
 * process:
 *   field_is_a_teapot:
 *     plugin: evaluate_condition
 *     source: my_source_url
 *     condition: http_status(418)
 * @endcode
 *
 * or even
 *
 * @code
 * process:
 *   field_is_a_teapot:
 *     plugin: evaluate_condition
 *     source: my_source_url
 *     condition:
 *       plugin: http_status
 *       code:
 *         - 418
 * @endcode
 *
 * @MigrateConditionsConditionPlugin(
 *   id = "http_status",
 *   parens = "code"
 * )
 */
class HttpStatus extends ConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Array of status codes to compare to.
   *
   * @var array|string[]
   */
  protected $statusCodes;

  /**
   * Constructs an HttpStatus plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    if (isset($configuration['code'])) {
      $this->statusCodes = is_array($configuration['code']) ? $configuration['code'] : [$configuration['code']];
    }
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doEvaluate($source, Row $row) {
    if ($this->statusCodes) {
      $options = [
        'http_errors' => FALSE,
      ];
      try {
        $response = $this->httpClient->head($source, $options);
        return in_array($response->getStatusCode(), $this->statusCodes);
      }
      catch (\Exception $e) {
        return FALSE;
      }
    }
    else {
      try {
        $this->httpClient->head($source);
        return TRUE;
      }
      catch (\Exception $e) {
        return FALSE;
      }
    }
  }

}
