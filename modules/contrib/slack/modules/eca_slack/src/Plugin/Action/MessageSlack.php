<?php

namespace Drupal\eca_slack\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Action\Plugin\Action\MessageAction;

/**
 * Send Slack a message, including tokens.
 *
 * @Action(
 *   id = "eca_slack_message",
 *   label = @Translation("Slack Message"),
 *   description = @Translation("Send Slack a message.")
 * )
 */
class MessageSlack extends MessageAction {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL): void {

    if (empty($this->configuration['node'])) {
      $this->configuration['node'] = $entity;
    }

    $channel = $this->configuration['channel'];
    $username = $this->configuration['username'];
    $message = $this->configuration['message'];

    $message = $this->token->replace($this->configuration['message'], $this->configuration);
    \Drupal::service('slack.slack_service')->sendMessage($message, $channel, $username);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'channel' => '',
      'username' => '',
      'message' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $site_slack_config = \Drupal::config('slack.settings');
    $site_slack_channel = $site_slack_config->get('slack_channel');
    $site_slack_username = $site_slack_config->get('slack_username');

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['channel'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Slack Channel'),
      '#description'   => $this->t('Slack channel to post in - site default is <strong>@site_slack_channel</strong>.', array('@site_slack_channel' => $site_slack_channel)),
      '#required'      => TRUE,
      '#default_value' => $site_slack_channel ? $site_slack_channel : $this->configuration['channel'],
      '#weight' => -30,
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slack (Bot) User Name'),
      '#description' => $this->t('Slack name to post under - site default is <strong>@site_slack_username</strong>.', array('@site_slack_username' => $site_slack_username)),
      '#default_value' => $site_slack_username ? $site_slack_username : $this->configuration['username'],
      '#weight' => -20,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message (mrkdwn format)'),
      '#description' => $this->t('<a href="https://api.slack.com/reference/surfaces/formatting" target="_blank">Slack Formatting Reference (mrkdwn)</a> - You can use tokens in this field.'),
      '#default_value' => $this->configuration['message'],
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['channel'] = $form_state->getValue('channel');
    $this->configuration['username'] = $form_state->getValue('username');
    $this->configuration['message'] = $form_state->getValue('message');
    parent::submitConfigurationForm($form, $form_state);
  }
}
