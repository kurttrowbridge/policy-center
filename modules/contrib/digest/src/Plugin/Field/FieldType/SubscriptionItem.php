<?php

namespace Drupal\digest\Plugin\Field\FieldType;

use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Defines a subscriptions field.
 *
 * This disallows the use of the subscription widget on non-subscription fields.
 *
 * @FieldType(
 *   id = "digest_subscription",
 *   label = @Translation("Digest subscription"),
 *   description = @Translation("A field to track subscriptions to digests."),
 *   category = @Translation("Digest"),
 *   module = "digest",
 *   default_widget = "digest_subscription_widget",
 *   default_formatter = "list_default",
 *   cardinality = \Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
 * )
 */
class SubscriptionItem extends ListStringItem {

}
