<?php

namespace Drupal\digest;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Builds a list of available Digests.
 */
class DigestListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function load() {

    $entities = [
      'enabled' => [],
      'disabled' => [],
    ];

    // Sort entities into enabled and disabled.
    foreach (parent::load() as $entity) {

      if ($entity->get('status')) {
        $entities['enabled'][] = $entity;
      }
      else {
        $entities['disabled'][] = $entity;
      }

    }

    return $entities;

  }

  /**
   * {@inheritDoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {

    $operations = parent::getDefaultOperations($entity);

    $operations['test'] = [
      'title' => 'Test',
      'url' => $entity->toUrl('test-form'),
      'weight' => $operations['edit']['weight'] + 5,
    ];

    return $operations;

  }

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {

    $header['title'] = $this->t('Title');
    $header['machine_name'] = $this->t('Machine Name');
    $header['description'] = $this->t('Description');

    return $header + parent::buildHeader();

  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {

    $row['title'] = $entity->get('title');
    $row['machine_name'] = $entity->get('id');
    $row['description'] = $entity->get('description');

    return $row + parent::buildRow($entity);

  }

  /**
   * {@inheritDoc}
   */
  public function render() {

    // Set up the headers and tables.
    $list = [
      'enabled' => [
        'heading' => [
          '#markup' => '<h2>' . $this->t('Enabled') . '</h2>',
        ],
        'table' => [
          '#type' => 'table',
          '#empty' => $this->t('There are no enabled digests.'),
        ],
      ],
      'disabled' => [
        'heading' => [
          '#markup' => '<h2>' . $this->t('Disabled') . '</h2>',
        ],
        'table' => [
          '#type' => 'table',
          '#empty' => $this->t('There are no disabled digests.'),
        ],
      ],
    ];

    $entities = $this->load();
    foreach (['enabled', 'disabled'] as $status) {

      $list[$status]['table'] += [
        '#header' => $this->buildHeader(),
      ];

      foreach ($entities[$status] as $entity) {

        $list[$status]['table']['#rows'][$entity->id()] = $this->buildRow($entity);

      }

    }

    return $list;

  }

}
