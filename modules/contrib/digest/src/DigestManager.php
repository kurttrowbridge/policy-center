<?php

namespace Drupal\digest;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * The digest manager class.
 */
class DigestManager implements DigestManagerInterface {

  /**
   * The digest storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $storage;

  /**
   * The digest send queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Digest module logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new DigestManager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, QueueFactory $queueFactory, ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $loggerFactory) {

    $this->storage = $entityTypeManager->getStorage('digest');
    $this->queue = $queueFactory->get('digest_send', TRUE);
    $this->configFactory = $configFactory;
    $this->logger = $loggerFactory->get('digest');

  }

  /**
   * {@inheritDoc}
   */
  public function getEnabled() {

    $digest_query = $this->storage->getQuery()
      ->condition('status', TRUE)
      ->accessCheck(TRUE);

    /** @var \Drupal\digest\Entity\DigestInterface[] $digests */
    $digests = $this->storage->loadMultiple($digest_query->execute());
    return $digests;

  }

  /**
   * {@inheritDoc}
   */
  public function getAsOptions() {

    $options = [];

    foreach ($this->getEnabled() as $digest) {

      $options[$digest->id()] = $digest->getTitle();

    }

    return $options;

  }

  /**
   * {@inheritDoc}
   */
  public function queueAll() {

    foreach ($this->getEnabled() as $digest) {

      // Ensure digest is correctly formed before queuing.
      $errors = [];
      if (!$digest->canSend($errors)) {

        // Log all found errors then skip to the next digest.
        foreach ($errors as $error) {

          $this->logger->warning("'" . $digest->getTitle()
            . "' produced warning: '" . $error
            . "'. This digest will not be queued.");

        }

        continue;

      }

      // Skip digests that should not be sent.
      if (!$digest->shouldSend()) {
        continue;
      }

      // Add each subscribed user to the queue.
      $user_ids = $digest->getSubscribedUserIds();

      foreach ($user_ids as $user_id) {

        $this->queue->createItem([
          'digest' => $digest->id(),
          'user' => $user_id,
        ]);

      }

      // Update the digests run date.
      $digest->setNextSendDate();

    }

  }

  /**
   * {@inheritDoc}
   */
  public function prepareMail($key, array $message, array $params) {

    // Use the digest from to send email.
    $message['from'] = $this->configFactory->get('digest.settings')
      ->get('digest_from_email');

    // Place the subject and body into the message.
    $message['subject'] = $params['subject'];
    $message['body'][] = $params['body'];

    return $message;

  }

}
