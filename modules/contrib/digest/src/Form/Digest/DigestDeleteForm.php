<?php

namespace Drupal\digest\Form\Digest;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting digest entities.
 *
 * @see \Drupal\digest\Entity\Digest
 */
class DigestDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @name digest?',
      ['@name' => $this->entity->label()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.digest.digest_display');
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    return $this->t('All user data associated with this digest will be removed,
      I.E. subscriptions. This action cannot be undone.');
  }

  /**
   * {@inheritDoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->entity->delete();
    $this->messenger()->addMessage($this->t('The @name digest has been deleted.',
      ['@name' => $this->entity->label()]));

    $form_state->setRedirectUrl($this->getCancelUrl());

  }

}
