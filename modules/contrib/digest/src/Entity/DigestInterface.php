<?php

namespace Drupal\digest\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface to interact with a Digest config entity.
 *
 * @see ConfigEntityInterface
 */
interface DigestInterface extends ConfigEntityInterface {

  /**
   * Returns the title of the digest.
   *
   * @return string
   *   The title of the digest.
   */
  public function getTitle();

  /**
   * Sets the title of the digest.
   *
   * @param string $setTo
   *   The value to set the title to.
   *
   * @return $this
   */
  public function setTitle($setTo);

  /**
   * Returns the digests description.
   *
   * @return string
   *   The digests description.
   */
  public function getDescription();

  /**
   * Sets the description of the digest.
   *
   * @param string $setTo
   *   The value to set the title to.
   *
   * @return $this
   */
  public function setDescription($setTo);

  /**
   * Returns the current schedule as a CronExpression.
   *
   * @return \Cron\CronExpression|null
   *   The CronExpression representation of the schedule.
   *   Returns NULL if the schedule is invalid or missing.
   */
  public function getSchedule();

  /**
   * Sets the schedule of the digest.
   *
   * @param string|\Cron\CronExpression $setTo
   *   The value to set the schedule to.
   * @param bool $updateSendDate
   *   Whether the send date should be updated.
   *
   * @return $this
   *
   * @throws \InvalidArgumentException
   *   Thrown when 'setTo' is an invalid string.
   *   The schedule will not be updated.
   */
  public function setSchedule($setTo, $updateSendDate = TRUE);

  /**
   * Gets the block plugin used for this digests display.
   *
   * @return \Drupal\Core\Block\BlockPluginInterface
   *   The block plugin for this digest.
   *   If the value for the display block ID is invalid the 'broken' block will
   *   be returned.
   */
  public function getDisplayBlock();

  /**
   * Sets the display block of the digest.
   *
   * @param string $setTo
   *   The ID of the block to use.
   *
   * @return $this
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown when the given block ID is not valid.
   */
  public function setDisplayBlock($setTo);

  /**
   * Determines if the digest is in a valid state to be sent.
   *
   * If the digest is mal-formed (no display, no title, etc.) then it should
   * not be queued or sent since an error will be raised.
   *
   * This does not tell you if the digest should be sent out or if it will
   * send. Transport or user errors may still occur during sending.
   *
   * @param array $diagnostic
   *   If provided, this array will be filled with ordered errors.
   *   This is indexed numerically and the array will be wiped of previous
   *   values.
   *   If this function returns true, this array will be empty.
   *
   * @return bool
   *   Whether the digest can be sent out.
   */
  public function canSend(&$diagnostic = NULL);

  /**
   * Determines if the digest should be sent out to users.
   *
   * @param \DateTimeInterface|string $relativeTo
   *   A PHP date string to base calculations off of. Defaults to now.
   *
   * @return bool
   *   Whether the digest is due.
   *
   * @throws \Exception
   *   Thrown if the value of 'relativeTo' is invalid.
   */
  public function shouldSend($relativeTo = 'now');

  /**
   * Builds an email and sends it out to the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to send this digest to.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   *   Thrown when a the digest cannot be sent out. For instance, there is no
   *   valid block to display.
   */
  public function send(UserInterface $user);

  /**
   * Retrieves the next time this digest should be sent.
   *
   * @return \DateTimeInterface|null
   *   The next time this digest should be sent.
   *   Will return NULL if the digest does not have a send date. E.g. If there
   *   is no valid schedule.
   */
  public function getNextSendDate();

  /**
   * Sets the next send date for this digest based of a given time.
   *
   * If this does not have a valid schedule then no time stamp is set.
   *
   * @param \DateTimeInterface|string $relativeTo
   *   A PHP date string to base calculations off of. Defaults to now.
   *
   * @return $this
   *
   * @throws \Exception
   *   Thrown if the value of 'relativeTo' is invalid.
   */
  public function setNextSendDate($relativeTo = 'now');

  /**
   * Returns a list of all IDs of users that are subscribed to this digest.
   *
   * @return int[]
   *   The IDs of users subscribed to this digest.
   */
  public function getSubscribedUserIds();

}
