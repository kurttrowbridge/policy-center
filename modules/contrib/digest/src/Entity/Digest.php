<?php

namespace Drupal\digest\Entity;

use Cron\CronExpression;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\user\UserInterface;

/**
 * Defines the base Digest config entity.
 *
 * @ConfigEntityType(
 *   id = "digest",
 *   label = @Translation("Digest"),
 *   module = "digest",
 *   config_prefix = "digest",
 *   admin_permission = "administer digest settings",
 *   handlers = {
 *     "list_builder" = "Drupal\digest\DigestListBuilder",
 *     "form" = {
 *       "add" = "Drupal\digest\Form\Digest\DigestEditForm",
 *       "edit" = "Drupal\digest\Form\Digest\DigestEditForm",
 *       "delete" = "Drupal\digest\Form\Digest\DigestDeleteForm",
 *       "test" = "Drupal\digest\Form\Digest\DigestTestForm",
 *     },
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/digest/{digest}/edit",
 *     "delete-form" = "/admin/structure/digest/{digest}/delete",
 *     "test-form" = "/admin/structure/digest/{digest}/test",
 *     "collection" = "/admin/structure/digests",
 *     "enable" = "/admin/structure/digest/{digest}/enable",
 *     "disable" = "/admin/structure/digest/{digest}/disable",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "status" = "status",
 *     "label" = "title",
 *   },
 *   config_export = {
 *     "id",
 *     "title",
 *     "description",
 *     "schedule",
 *     "display_block",
 *   },
 * )
 *
 * @see ConfigEntityBase
 * @see DigestInterface
 */
class Digest extends ConfigEntityBase implements DigestInterface {

  /**
   * The post fix to use for storing the digests next send time stamp.
   */
  public const STATE_POST_FIX = '_next_send_date';

  /**
   * ID of the digest.
   *
   * @var string
   */
  protected $id;

  /**
   * The title of the digest.
   *
   * @var string
   */
  protected $title;

  /**
   * The description of the digest.
   *
   * @var string
   */
  protected $description;

  /**
   * The raw value of the cron expression for this digest.
   *
   * @var string
   */
  protected $schedule;

  /**
   * The ID block used to create the body of this digest.
   *
   * @var string
   */
  protected $display_block;

  /**
   * The block plugin manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $values, $entity_type) {

    parent::__construct($values, $entity_type);

    $this->blockManager = \Drupal::service('plugin.manager.block');
    $this->state = \Drupal::state();
    $this->mailManager = \Drupal::service('plugin.manager.mail');
    $this->renderer = \Drupal::service('renderer');

  }

  /**
   * Ensures no state is left over when new digests are removed.
   */
  public function __destruct() {

    // No clean up needs to happen on saved digests.
    if (!$this->isNew()) {
      return;
    }

    // Remove any state from this digest.
    $this->state->delete($this->getStateKeyName());

  }

  /**
   * {@inheritDoc}
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * {@inheritDoc}
   */
  public function setTitle($setTo) {
    $this->title = $setTo;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritDoc}
   */
  public function setDescription($setTo) {
    $this->description = $setTo;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getSchedule() {

    try {
      return new CronExpression($this->schedule);
    }
    catch (\Throwable $exception) {
      return NULL;
    }

  }

  /**
   * {@inheritDoc}
   */
  public function setSchedule($setTo, $updateSendDate = TRUE) {

    if (is_string($setTo)) {

      // An exception will be thrown if the value of setTo is invalid.
      $setTo = new CronExpression($setTo);

    }

    $this->schedule = $setTo->getExpression();

    // Update the next send timestamps.
    if ($updateSendDate) {
      $this->setNextSendDate();
    }

    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function getDisplayBlock() {
    return $this->blockManager->createInstance($this->display_block);
  }

  /**
   * {@inheritDoc}
   */
  public function setDisplayBlock($setTo) {

    if (!$this->blockManager->hasDefinition($setTo)) {
      throw new PluginException('The ID ' . $setTo . ' does not exist.');
    }

    // Update the block ID and instance.
    $this->display_block = $setTo;

    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function canSend(&$diagnostic = NULL) {

    $report = !is_null($diagnostic);
    $internal_diagnostic = [];
    $index = 0;

    // Check health of the digest.
    if (empty($this->getTitle())) {
      $internal_diagnostic[$index++] = 'No title is set';
    }

    if (is_null($this->getSchedule())) {
      $internal_diagnostic[$index++] = 'No schedule is set';
    }

    if ($this->getDisplayBlock()->getPluginId() == 'broken') {
      $internal_diagnostic[$index++] = 'No display is set';
    }

    if ($report) {
      $diagnostic = $internal_diagnostic;
    }

    // If no errors were found the digest can be sent.
    return empty($internal_diagnostic);

  }

  /**
   * {@inheritDoc}
   */
  public function shouldSend($relativeTo = 'now') {

    // This should not send if there is no valid schedule or it is disabled.
    if (!$this->getSchedule() || !$this->status()) {
      return FALSE;
    }

    if (is_string($relativeTo)) {
      $relativeTo = new \DateTime($relativeTo);
    }

    $next_send = $this->getNextSendDate();

    // A digest should send if it is past the next scheduled date for it to run.
    return $next_send->getTimestamp() < $relativeTo->getTimestamp();

  }

  /**
   * {@inheritDoc}
   */
  public function send(UserInterface $user) {

    $block = $this->getDisplayBlock();
    if ($block->getPluginId() == 'broken') {
      // If no valid block was set, throw an exception.
      throw new EntityMalformedException('No valid display block given to this digest.');
    }

    // Switch to the recipients account to avoid permission issues (e.g., when
    // sending during CRON).
    /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
    $account_switcher = \Drupal::service('account_switcher');
    $account_switcher->switchTo($user);

    $block_renderer = \Drupal::service('block_renderer');
    $block_build = $block_renderer->renderPluginBlock($block->getPluginId());

    $body = $this->renderer->renderPlain($block_build);

    if ($body) {
      $this->mailManager->mail(
          'digest',
          $this->id(),
          $user->getEmail(),
          $user->getPreferredLangcode(),
          [
            'subject' => $this->getTitle(),
            'body' => $body,
          ]
        );
    }

    $account_switcher->switchBack();

  }

  /**
   * {@inheritDoc}
   */
  public function getNextSendDate() {

    // Ensure that there is a valid schedule.
    if (!$this->getSchedule()) {
      return NULL;
    }

    $from_state = $this->state->get($this->getStateKeyName());

    if (!isset($from_state)) {

      // If no send date is set, we assume it needs to be set.
      $this->setNextSendDate();

      // Since the state is now set, we can get the value.
      $from_state = $this->state->get($this->getStateKeyName());

    }

    // In the state, this is stored as a time stamp so it must be converted to a
    // Date time object.
    return new \DateTime('@' . $from_state);

  }

  /**
   * {@inheritDoc}
   */
  public function setNextSendDate($relativeTo = 'now') {

    // Ensure there is a schedule.
    $schedule = $this->getSchedule();
    if (!isset($schedule)) {
      return $this;
    }

    // The next send date should follow the schedule.
    $next_run = $schedule->getNextRunDate($relativeTo);

    // Store the timestamp into state.
    $this->state->set($this->getStateKeyName(), $next_run->getTimestamp());

    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function getSubscribedUserIds() {

    $user_storage = $this->entityTypeManager()->getStorage('user');
    $query = $user_storage->getQuery()
      ->condition('field_digest_subscriptions', $this->id())
      ->accessCheck(TRUE);

    return $query->execute();

  }

  /**
   * {@inheritDoc}
   */
  public function set($property_name, $value) {

    parent::set($property_name, $value);

    // Update the send date if the schedule is updated.
    if ($property_name == 'schedule') {
      $this->setNextSendDate();
    }

    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function delete() {

    // Remove all user subscriptions to this digest.
    $user_storage = $this->entityTypeManager()->getStorage('user');
    $user_ids = $this->getSubscribedUserIds();

    foreach ($user_ids as $user_id) {

      /** @var \Drupal\user\UserInterface $user */
      $user = $user_storage->load($user_id);

      // Get the subscription field and values.
      $subscription_field = $user->get('field_digest_subscriptions');
      $subscriptions = $subscription_field->getValue();

      // Find the position of the digest.
      $key = array_search($this->id(), array_column($subscriptions, 'value'));

      // Remove the digest from the list.
      $subscription_field->removeItem($key);
      $user->save();

    }

    // Remove the state for this digest.
    $this->state->delete($this->getStateKeyName());

    parent::delete();

  }

  /**
   * Gets the name for the state key of this digests timestamp.
   *
   * @return string
   *   The key to use in the state.
   */
  protected function getStateKeyName() {
    return 'digest.' . $this->id() . self::STATE_POST_FIX;
  }

}
