<?php

namespace Drupal\Tests\digest\Kernel\Entity;

use Cron\CronExpression;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the digest entity.
 *
 * @coversDefaultClass \Drupal\digest\Entity\Digest
 *
 * @group digest
 */
class DigestTest extends EntityKernelTestBase {

  /**
   * The digest storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $digestStorage;

  /**
   * A digest.
   *
   * @var \Drupal\digest\Entity\Digest
   */
  protected $digest;

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'block_renderer',
    'digest',
    'digest_test',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('digest');

    $this->digestStorage = $this->entityTypeManager->getStorage('digest');

    $this->digest = $this->digestStorage->create([
      'id' => 'my_digest',
    ]);
    $this->digest->save();

  }

  /**
   * Tests the title helper methods.
   *
   * @covers ::getTitle
   * @covers ::setTitle
   */
  public function testTitle() {

    $this->assertNull($this->digest->getTitle());

    $title = $this->randomMachineName();
    $this->digest->setTitle($title);
    $this->assertEquals($title, $this->digest->getTitle());

  }

  /**
   * Tests the description helper methods.
   *
   * @covers ::getDescription
   * @covers ::setDescription
   */
  public function testDescription() {

    $this->assertNull($this->digest->getDescription());

    $description = $this->randomMachineName();
    $this->digest->setDescription($description);
    $this->assertEquals($description, $this->digest->getDescription());

  }

  /**
   * Tests the schedule helpers methods.
   *
   * @covers ::getSchedule
   * @covers ::setSchedule
   */
  public function testSchedule() {

    $valid_cron = '* * * * *';

    // Getting the schedule before it has been set should return null.
    $this->assertNull($this->digest->getSchedule());

    // An invalid cron value should also return NULL.
    $this->digest->set('schedule', 'Not cron');
    $this->assertNull($this->digest->getSchedule());

    // Should successfully set and retrieve a cron expression.
    $this->digest->setSchedule($valid_cron);
    $expression = $this->digest->getSchedule()->getExpression();
    $this->assertEquals($valid_cron, $expression);

    try {

      // An exception should be thrown for invalid cron strings.
      $this->digest->setSchedule('I am not cron');
      $this->fail('No exception thrown for invalid cron expression.');

    }
    catch (\InvalidArgumentException $exception) {

      // The schedule should not have changed.
      $expression = $this->digest->getSchedule()->getExpression();
      $this->assertEquals($valid_cron, $expression);

    }

    // The correct expression should be returned after updating the value
    // through generic entity methods.
    $valid_cron = '0 0 * * 5';
    $this->digest->set('schedule', $valid_cron);
    $expression = $this->digest->getSchedule();
    $this->assertEquals($valid_cron, $expression);

    // The same results should occur if a CronExpression object is passed in.
    $cron_expression = new CronExpression($valid_cron);
    $this->digest->setSchedule($cron_expression);
    $expression = $this->digest->getSchedule();
    $this->assertEquals($cron_expression, $expression);

    // The next send date should be updated when changing the schedule.
    $valid_cron = '0 0 * * 3';
    $old_next_send = $this->digest->getNextSendDate();
    $this->digest->setSchedule($valid_cron);
    $new_next_send = $this->digest->getNextSendDate();
    $this->assertNotEquals($old_next_send, $new_next_send);

    // The next send date should not be updated if specified not to.
    $old_next_send = $new_next_send;
    $valid_cron = '0 0 * * 4';
    $this->digest->setSchedule($valid_cron, FALSE);
    $new_next_send = $this->digest->getNextSendDate();
    $this->assertEquals($old_next_send, $new_next_send);

  }

  /**
   * Tests the display block helpers methods.
   *
   * @covers ::getDisplayBlock
   * @covers ::setDisplayBlock
   */
  public function testDisplayBlock() {

    $valid_block = 'page_title_block';

    // An invalid or missing block ID should load the broken block.
    $this->assertEquals('broken', $this->digest->getDisplayBlock()->getPluginId());

    // Should successfully set and retrieve the display block.
    $this->digest->setDisplayBlock($valid_block);
    $block = $this->digest->getDisplayBlock();
    $this->assertEquals($valid_block, $block->getPluginId());

    try {

      // An exception should be thrown for invalid IDs.
      $this->digest->setDisplayBlock('Not a block');
      $this->fail('No exception thrown for invalid block ID.');

    }
    catch (PluginException $exception) {

      // The display block should not have changed.
      $block = $this->digest->getDisplayBlock();
      $this->assertEquals($valid_block, $block->getPluginId());

    }

    // The correct block should be returned after updating the value through
    // generic entity methods.
    $valid_block = 'user_login_block';
    $this->digest->set('display_block', $valid_block);
    $block = $this->digest->getDisplayBlock();
    $this->assertEquals($valid_block, $block->getPluginId());

  }

  /**
   * Ensures that digests appropriately asses whether they can be sent out.
   *
   * @covers ::canSend
   */
  public function testCanSend() {

    $diagnostic = [];

    // A digest cannot be sent without a title, schedule, or body.
    $this->assertFalse($this->digest->canSend($diagnostic));
    $this->assertCount(3, $diagnostic);

    $this->assertStringContainsString('title', $diagnostic[0]);
    $this->assertStringContainsString('schedule', $diagnostic[1]);
    $this->assertStringContainsString('display', $diagnostic[2]);

    // Add required data to digest and ensure it can now send.
    $this->digest->setTitle('My Digest');
    $this->digest->setSchedule('* * * * *');
    $this->digest->setDisplayBlock('test_admin_block');

    $this->assertTrue($this->digest->canSend());

  }

  /**
   * Tests the should send functionality of a digest.
   *
   * @covers ::shouldSend
   */
  public function testShouldSend() {

    // A digest with no valid or set schedule should never be sent.
    $this->assertFalse($this->digest->shouldSend());

    // Send this digest Mondays at midnight.
    $expression = new CronExpression('0 0 * * 1');
    $this->digest->setSchedule($expression);

    // Digests should not send as soon as they have a valid expression.
    $this->assertFalse($this->digest->shouldSend());

    // Once the next scheduled time is reached the digest should send.
    $future_time = 'next tuesday';
    $this->assertTrue($this->digest->shouldSend($future_time));

    // An invalid date string should generate an exception.
    try {

      $this->digest->shouldSend('Not a date string.');
      $this->fail('No exception thrown for an invalid date string.');

    }
    catch (\Exception $exception) {
    }

    // The relative time should accept a Date Time object as well.
    $future_time = new \DateTime($future_time);
    $this->assertTrue($this->digest->shouldSend($future_time));

    // Disabled digests should never send.
    $this->digest->disable();
    $this->assertFalse($this->digest->shouldSend($future_time));

  }

  /**
   * Tests the sending of digests.
   *
   * @covers ::send
   */
  public function testSend() {

    // Add some values to the digest.
    $this->digest->setTitle('My Digest');

    // Grab the state.
    $state = \Drupal::state();

    // Set the digest from email.
    $from_email = 'test@example.com';
    $this->config('digest.settings')
      ->set('digest_from_email', $from_email)
      ->save();

    // Create a non admin user.
    $non_admin_user = $this->drupalCreateUser([], NULL, FALSE, [
      'mail' => 'digest@example.com',
      'preferred_langcode' => 'en',
    ]);

    // Cannot send a digest that does not have a display.
    try {

      $this->digest->send($non_admin_user);
      $this->fail('No exception thrown for a digest sending with no display.');

    }
    catch (EntityMalformedException $exception) {

      // Ensure no emails were sent.
      $this->assertEmpty($state->get('system.test_mail_collector'));

    }

    // Send a digest with a valid display.
    $this->digest->setDisplayBlock('test_admin_block');
    $this->digest->send($non_admin_user);

    // Check the email sent.
    $mail = $state->get('system.test_mail_collector');
    $this->assertCount(1, $mail);
    $mail = reset($mail);

    // Check the details of the mail are correct.
    $this->assertEquals($non_admin_user->getEmail(), $mail['to']);
    $this->assertEquals($non_admin_user->getPreferredLangcode(), $mail['langcode']);

    // The email should have used the digest settings value.
    $this->assertEquals($from_email, $mail['from']);

    // The subject should contain the title by default.
    $this->assertStringContainsString($this->digest->getTitle(), $mail['subject']);

    // Check that the body has the rendered block in it.
    $this->assertStringContainsString('Not admin.', $mail['body']);

    // Clear out the mail state.
    $state->delete('system.test_mail_collector');

    // Send the digest to an admin user to ensure the block renders
    // differently.
    $admin_user = $this->drupalCreateUser([], NULL, FALSE, [
      'mail' => 'digestadmin@example.com',
      'preferred_langcode' => 'en',
      'roles' => [
        'admin',
      ],
    ]);
    $this->digest->send($admin_user);

    // Check the email sent.
    $mail = $state->get('system.test_mail_collector');
    $this->assertCount(1, $mail);
    $mail = reset($mail);

    // Check that the body has the rendered block in it as viewed by an admin.
    $this->assertStringContainsString('Admin.', $mail['body']);

  }

  /**
   * Tests retrieving and setting the next sent date of the digest.
   *
   * @covers ::getNextSendDate
   * @covers ::setNextSendDate
   */
  public function testNextSendDate() {

    // A digest with no schedule should return null.
    $this->assertNull($this->digest->getNextSendDate());

    // Send this digest Mondays at midnight.
    $expression = new CronExpression('0 0 * * 1');
    $this->digest->setSchedule($expression);

    // After setting a digests schedule, the next send date should be it's next
    // scheduled send date.
    $this->assertEquals($this->digest->getNextSendDate(), $expression->getNextRunDate());

    // After setting the next send date, that next run date (according to the
    // schedule) should be returned.
    // Set as a string.
    $next_send = 'next tuesday';
    $this->digest->setNextSendDate($next_send);
    $this->assertEquals($this->digest->getNextSendDate(), $expression->getNextRunDate($next_send));

    // An invalid string should throw an exception.
    try {

      $this->digest->setNextSendDate('Not a date.');
      $this->fail('No exception thrown for an invalid date string.');

    }
    catch (\Exception $exception) {

      // The value of the next send date should not have changed.
      $this->assertEquals($this->digest->getNextSendDate(), $expression->getNextRunDate($next_send));

    }

    // Set as a DateTime object.
    $next_send = new \DateTime('next wednesday');
    $this->digest->setNextSendDate($next_send);
    $this->assertEquals($this->digest->getNextSendDate(), $expression->getNextRunDate($next_send));

    // Set as default. Should match the schedule for next send date.
    $this->digest->setNextSendDate();
    $this->assertEquals($this->digest->getNextSendDate(), $expression->getNextRunDate());

  }

  /**
   * Tests getting the users subscribed to a digest.
   *
   * @covers ::getSubscribedUserIds
   */
  public function testGetSubscribedUserIds() {

    // Install the config to turn on the subscription field.
    $this->installConfig('digest');

    // No users should be subscribed.
    $this->assertCount(0, $this->digest->getSubscribedUserIds());

    $user_1 = $this->drupalCreateUser([], 'user 1', FALSE, [
      'field_digest_subscriptions' => $this->digest->id(),
    ]);

    // There should now be one subscribed user.
    $user_ids = $this->digest->getSubscribedUserIds();
    $this->assertCount(1, $user_ids);
    $this->assertEquals($user_1->id(), reset($user_ids));

    // Create a user that is not subscribed to this digest.
    $this->createUser([
      'field_digest_subscriptions' => 'not the same digest',
    ]);

    // There should still only be one subscribed user.
    $user_ids = $this->digest->getSubscribedUserIds();
    $this->assertCount(1, $user_ids);

    $this->assertEquals($user_1->id(), reset($user_ids));

  }

  /**
   * Tests that using the set method will apply side effects for certain values.
   *
   * @covers ::set
   */
  public function testSetting() {

    $this->digest->setSchedule('0 0 * * 5');
    $old_next_send = $this->digest->getNextSendDate();

    // The next send date should be updated if the schedule is set.
    $valid_cron = '0 0 * * 1';
    $this->digest->set('schedule', $valid_cron);
    $new_next_send = $this->digest->getNextSendDate();
    $this->assertNotEquals($old_next_send, $new_next_send);

  }

  /**
   * Tests that all relevant data is removed from a site when deleting a digest.
   *
   * @covers ::delete
   */
  public function testDeletion() {

    // Install the config to turn on the subscription field.
    $this->installConfig('digest');

    // Subscribe a user to the digest.
    $user = $this->createUser([
      'field_digest_subscriptions' => [
        $this->digest->id(),
        'some_other_digest',
      ],
    ]);
    $user->save();

    // Delete the digest.
    $this->digest->delete();

    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    // Make sure the users field data for this digest was removed.
    $subscribed_user_count = $user_storage->getQuery()
      ->condition('field_digest_subscriptions', $this->digest->id())
      ->count()
      ->accessCheck(TRUE)
      ->execute();
    $this->assertEquals(0, $subscribed_user_count);

    // Make sure other subscriptions are still intact.
    $subscribed_user_count = $user_storage->getQuery()
      ->condition('field_digest_subscriptions', 'some_other_digest')
      ->count()
      ->accessCheck(TRUE)
      ->execute();
    $this->assertEquals(1, $subscribed_user_count);

  }

}
