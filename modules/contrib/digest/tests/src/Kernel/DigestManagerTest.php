<?php

namespace Drupal\Tests\digest\Kernel;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Psr\Log\LoggerInterface;

/**
 * Tests for the digest manager class.
 *
 * @group digest
 *
 * @coversDefaultClass \Drupal\digest\DigestManager
 *
 * @see \Drupal\digest\DigestManager
 */
class DigestManagerTest extends EntityKernelTestBase {

  /**
   * The digest manager.
   *
   * @var \Drupal\digest\DigestManagerInterface
   */
  protected $manager;

  /**
   * The digest storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
    'digest_test',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {

    parent::setUp();

    $this->installEntitySchema('digest');

    $this->manager = \Drupal::service('digest.digest_manager');
    $this->storage = \Drupal::entityTypeManager()->getStorage('digest');

  }

  /**
   * Tests getting all enabled digests.
   *
   * @covers ::getEnabled
   */
  public function testGetEnabled() {

    // No digests are created so there should be no enabled ones.
    $this->assertCount(0, $this->manager->getEnabled());

    $digest_1 = $this->storage->create([
      'id' => 'digest_1',
    ]);
    $digest_1->save();

    // The enabled digest should be returned.
    $enabled = $this->manager->getEnabled();
    $this->assertCount(1, $enabled);
    $this->assertEquals($digest_1->id(), reset($enabled)->id());

    $this->storage->create([
      'id' => 'digest_2',
      'status' => FALSE,
    ])->save();

    // The new digest should not be returned.
    $enabled = $this->manager->getEnabled();
    $this->assertCount(1, $enabled);
    $this->assertEquals($digest_1->id(), reset($enabled)->id());

  }

  /**
   * Tests getting digests as options.
   *
   * @covers ::getAsOptions
   */
  public function testGetAsOptions() {

    // When there are no digests there should be no options.
    $options = $this->manager->getAsOptions();
    $this->assertCount(0, $options);

    /** @var \Drupal\digest\Entity\DigestInterface $digest_1 */
    $digest_1 = $this->storage->create([
      'id' => 'digest_1',
      'title' => 'My digest',
    ]);
    $digest_1->save();

    $digest_2 = $this->storage->create([
      'id' => 'digest_2',
      'status' => FALSE,
      'title' => 'A disabled digest',
    ]);
    $digest_2->save();

    // Only active digests should be options.
    $options = $this->manager->getAsOptions();
    $this->assertCount(1, $options);

    // Check that the options were created correctly.
    $this->assertEquals($options[$digest_1->id()], $digest_1->getTitle());
    $this->assertFalse(isset($options[$digest_2->id()]));

  }

  /**
   * Tests the queuing of digests.
   *
   * @covers ::queueAll
   */
  public function testQueueAll() {

    // Set up a mock logger to test for errors.
    $logger = $this->createMock(LoggerInterface::class);
    $logger->expects($this->atLeastOnce())
      ->method('log')
      ->with($this->logicalOr(
        RfcLogLevel::WARNING, $this->stringContains('title'),
        RfcLogLevel::WARNING, $this->stringContains('schedule'),
        RfcLogLevel::WARNING, $this->stringContains('display')
      ));

    /** @var \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory */
    $logger_factory = \Drupal::service('logger.factory');
    $logger_factory->addLogger($logger);

    $this->manager = \Drupal::service('digest.digest_manager');

    $queue = \Drupal::queue('digest_send');
    $queue->createQueue();

    // No digests were created so the queue should remain empty.
    $this->manager->queueAll();
    $this->assertEquals(0, $queue->numberOfItems());

    // Create digests to queue.
    /** @var \Drupal\digest\Entity\DigestInterface $digest_1 */
    $digest_1 = $this->storage->create([
      'id' => 'digest_1',
      'title' => 'Queue me',
      'schedule' => '0 0 * * 5',
      'display_block' => 'test_admin_block',
    ]);
    $digest_1->save();

    /** @var \Drupal\digest\Entity\DigestInterface $digest_2 */
    $digest_2 = $this->storage->create([
      'id' => 'digest_2',
      'status' => FALSE,
      'schedule' => '0 0 * * 5',
    ]);
    $digest_2->save();

    /** @var \Drupal\digest\Entity\DigestInterface $digest_3 */
    $digest_3 = $this->storage->create([
      'id' => 'digest_3',
      'description' => 'Malformed!',
    ]);
    $digest_3->save();

    // Create a user for subscribing.
    $this->installConfig('digest');
    $user = $this->createUser([
      'field_digest_subscriptions' => [
        $digest_1->id(),
        $digest_3->id(),
      ],
    ]);

    // Set the last send date relative to 0 to ensure that this should run.
    $digest_1->setNextSendDate('@0');
    $digest_3->setNextSendDate('@0');

    // One digest should have been queued.
    $this->manager->queueAll();
    $this->assertEquals(1, $queue->numberOfItems());

    // Check that the data was created correctly.
    $data = $queue->claimItem()->data;
    $this->assertEquals($digest_1->id(), $data['digest']);
    $this->assertEquals($user->id(), $data['user']);

    // Check that the send date of the digest has been updated.
    $this->assertFalse($digest_1->shouldSend());
    $this->assertEquals($digest_1->getNextSendDate(), $digest_1->getSchedule()->getNextRunDate());

  }

  /**
   * Tests the preparing of digests mail.
   *
   * @covers ::prepareMail
   */
  public function testPrepareMail() {

    // Set up the data for the email.
    $key = 'my_digest';
    $message = [];
    $params = [
      'subject' => 'Subject Line',
      'body' => 'Pre rendered content.',
    ];

    // Set the digest from email.
    $from_email = 'test@example.com';
    $this->config('digest.settings')
      ->set('digest_from_email', $from_email)
      ->save();

    $new_message = $this->manager->prepareMail($key, $message, $params);

    $this->assertEquals($from_email, $new_message['from']);
    $this->assertEquals($params['subject'], $new_message['subject']);
    $this->assertEquals($params['body'], $new_message['body'][0]);

  }

}
