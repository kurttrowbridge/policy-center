<?php

namespace Drupal\Tests\digest\Functional\Form\Digest;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Lorisleiva\CronTranslator\CronTranslator;

/**
 * Tests for the Digest edit form.
 *
 * @group digest
 *
 * @see \Drupal\digest\Form\Digest\DigestEditForm
 */
class DigestEditFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $adminUser;

  /**
   * {@inheritDoc}
   */
  public function setUp() : void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer digest settings',
    ]);

  }

  /**
   * Tests the form for a new digest.
   *
   * @covers \Drupal\digest\Form\Digest\DigestEditForm::form
   * @covers \Drupal\digest\Form\Digest\DigestEditForm::save
   */
  public function testAddNewDigest() {

    $this->drupalLogin($this->adminUser);

    // Open the add form.
    $this->drupalGet(Url::fromRoute('entity.digest.add_form'));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldEnabled('id');
    $this->assertSession()->pageTextNotContains('Delete');

    // Fill out form.
    $page = $this->getSession()->getPage();
    $page->fillField('id', strtolower($this->randomMachineName()));

    $titleUsed = $this->randomMachineName();
    $page->fillField('title', $titleUsed);

    $scheduleUsed = '* * * * *';
    $page->fillField('schedule', $scheduleUsed);

    $blockUsed = 'page_title_block';
    $page->fillField('display_block', $blockUsed);

    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('The ' . $titleUsed . ' digest has been created');

    // Check the corresponding entity was created.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $digests = $digest_storage->loadMultiple();

    $this->assertCount(1, $digests);
    $digest = reset($digests);
    $this->assertEquals($digest->get('title'), $titleUsed);
    $this->assertEquals($digest->get('schedule'), $scheduleUsed);
    $this->assertEquals($digest->get('display_block'), $blockUsed);

  }

  /**
   * Test the editing of an existing digest.
   *
   * @covers \Drupal\digest\Form\Digest\DigestEditForm::form
   */
  public function testEditDigest() {

    $this->drupalLogin($this->adminUser);

    // Create dummy digests.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $digest = $digest_storage->create([
      'id' => strtolower($this->randomMachineName()),
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(24),
      'schedule' => '* * * * *',
      'display_block' => 'page_title_block',
    ]);
    $digest->save();

    // Open the editing form.
    $this->drupalGet(Url::fromRoute('entity.digest.edit_form',
      ['digest' => $digest->id()]));
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldDisabled('id');
    $this->assertSession()->pageTextContains(CronTranslator::translate('* * * * *'));

    // Fill in and submit the form.
    $newTitle = $this->randomMachineName();
    $this->getSession()->getPage()->fillField('title', $newTitle);

    $this->submitForm([], 'Save');
    $this->assertSession()->pageTextContains('The ' . $newTitle . ' digest has been updated');

    // Check the corresponding entity was updated.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $digests = $digest_storage->loadMultiple();

    $this->assertCount(1, $digests);
    $digest = reset($digests);
    $this->assertEquals($digest->get('title'), $newTitle);

  }

}
