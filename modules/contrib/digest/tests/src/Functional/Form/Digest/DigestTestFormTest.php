<?php

namespace Drupal\Tests\digest\Functional\Form\Digest;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the digest testing form.
 *
 * @group digest
 *
 * @see \Drupal\digest\Form\Digest\DigestTestForm
 */
class DigestTestFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * A testing digest.
   *
   * @var \Drupal\digest\Entity\Digest
   */
  protected $digest;

  /**
   * {@inheritDoc}
   */
  protected function setUp() : void {

    parent::setUp();

    // Create and sign in as an admin.
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer digest settings',
    ]);
    $this->drupalLogin($this->adminUser);

    // Create a digest.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $this->digest = $digest_storage->create([
      'id' => strtolower($this->randomMachineName()),
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(24),
      'schedule' => '* * * * *',
      'display_block' => 'page_title_block',
    ]);
    $this->digest->save();

  }

  /**
   * Tests the display and functionality for the form.
   */
  public function testForm() {

    // Access the form.
    $this->drupalGet($this->digest->toUrl('test-form'));
    $this->assertSession()->statusCodeEquals(200);

    /** @var \Drupal\Core\Session\AccountProxyInterface $current_user */
    $current_user = \Drupal::service('current_user');

    // Ensure the current user is used as the default field value.
    $user_field = 'send_to_user';
    $user_field_value = $current_user->getAccountName() . ' (' . $current_user->id() . ')';
    $this->assertSession()->fieldExists($user_field);
    $this->assertSession()
      ->fieldValueEquals($user_field, $user_field_value);

    // Send the digest.
    $this->submitForm([], 'Send');

    $this->assertSession()->pageTextContains('The digest was sent successfully.');

    // Ensure email was sent properly.
    $mail = \Drupal::state()->get('system.test_mail_collector');
    $this->assertCount(1, $mail);
    $mail = reset($mail);

    $this->assertEquals($this->adminUser->getEmail(), $mail['to']);

  }

  /**
   * Ensure that invalid digests display errors when trying to send emails.
   */
  public function testInvalidDigest() {

    // Create an invalid digest.
    $digest_storage = \Drupal::entityTypeManager()->getStorage('digest');
    $this->digest = $digest_storage->create([
      'id' => strtolower($this->randomMachineName()),
      'title' => $this->randomMachineName(),
      'description' => $this->randomString(24),
      'schedule' => '* * * * *',
      'display_block' => 'not_a_block',
    ]);
    $this->digest->save();

    // Access the form.
    $this->drupalGet($this->digest->toUrl('test-form'));
    $this->assertSession()->statusCodeEquals(200);

    // Send the digest.
    $this->submitForm([], 'Send');

    $this->assertSession()->pageTextContains('The digest could not be sent.');

    // Ensure email was sent properly.
    $mail = \Drupal::state()->get('system.test_mail_collector');
    $this->assertEmpty($mail);

  }

}
