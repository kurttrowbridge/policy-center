<?php

namespace Drupal\Tests\digest\Functional\Form;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the configuration form for the Digest module.
 *
 * @group digest
 *
 * @see \Drupal\digest\Form\DigestSettingsForm
 */
class DigestSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'digest',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $adminUser;

  /**
   * {@inheritDoc}
   */
  public function setUp() : void {

    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer digest settings',
    ]);

  }

  /**
   * Tests that the settings form is displayed and submits data correctly.
   */
  public function testSettingsForm() {

    $this->drupalLogin($this->adminUser);

    // Load settings form.
    $this->drupalGet(Url::fromRoute('digest.digest_settings'));
    $this->assertSession()->statusCodeEquals(200);

    $config = $this->config('digest.settings');

    // Check for default values.
    $this->assertSession()->fieldValueEquals('send_from_email',
      $config->get('digest_from_email'));

    // Fill out and submit form.
    $page = $this->getSession()->getPage();
    $page->fillField('send_from_email', 'test@digest.com');
    $page->pressButton('Save configuration');

    // Check for new values.
    $this->drupalGet(Url::fromRoute('digest.digest_settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals('send_from_email', 'test@digest.com');

  }

}
