<?php

namespace Drupal\digest_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A block that tells you if the current user has the 'admin' role.
 *
 * @Block(
 *   id = "test_admin_block",
 *   admin_label = @Translation("Test admin block"),
 * )
 */
class TestAdminBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->currentUser = $container->get('current_user');

    return $instance;

  }

  /**
   * {@inheritDoc}
   */
  public function build() {

    $build = [
      '#markup' => 'Not admin.',
    ];

    if (in_array('admin', $this->currentUser->getRoles())) {
      $build['#markup'] = 'Admin.';
    }

    return $build;

  }

}
