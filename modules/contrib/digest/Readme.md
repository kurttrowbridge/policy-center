# Digest

Digest is a module built to provide an easy way to set up notifications about new pieces of content.

## Contents

* [Introduction](#introduction).
* [Requirements](#requirements).
* [Installation](#installation).
* [Configuration](#configuration).
* [Maintainers](#maintainers).

## Introduction

Digest allows content to be specified for release in a digest like newsletter.

A digest uses a block to render and then sends emails out to users who wish to receive notifications. The timing of when these are sent is adjustable by an admin.
This can be in the format of a stand-alone post or bundled together as a digest of new posts.

Visit the [project page](https://www.drupal.org/project/digest) for more information on releases, documentation, and issues. To submit bug reports, feature requests, or other issues visit the [issue queue](https://www.drupal.org/project/issues/digest).

## Requirements

The Digest module requires the use of blocks, and an outgoing email service such as [swiftmailer](https://www.drupal.org/project/swiftmailer).

## Installation

Install as you would install any other [contributed Drupal module](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) using [Composer](https://getcomposer.org) :

1. Run `composer require drupal/digest` (assuming you have set up the Drupal packages repository).

2. Enable the module in the 'Extend' menu (`admin/extend`) or via the command line with drush (`drush en digest`).

## Configuration

After installing and enabling the module, navigate to `/admin/config/people/accounts/form-display` and enable the "Digest subscriptions" field. Make sure to set the widget of that to "Digest subscriptions" as well.\
This allows users to edit their subscription preferences from the user edit page.

To create a new digest navigate to `/admin/structure/digests` and press the "Add digest" button.\
Any enabled digests will show up for users to subscribe to.

## Maintainers

Current Maintainers :

* Derek Cresswell - [derekcresswell](https://www.drupal.org/u/derekcresswell)

This project is sponsored by :

* [Acro Media Inc.](https://www.acromedia.com/)
    * A leading ecommerce service provider, giving the insights & development online retailers need to optimize their technology for scalable growth and innovation.
