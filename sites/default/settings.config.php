<?php

if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
    if (substr($_ENV['PANTHEON_ENVIRONMENT'], 0, 2) == 'p-') {
        $env = 'multidev';
    }
    else {
        $env = $_ENV['PANTHEON_ENVIRONMENT'];
    }
}
else {
    $env = 'local';
}

switch ($env) {
    case 'live':
        $config['environment_indicator.indicator']['bg_color'] = '#C4193E';
        $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
        $config['environment_indicator.indicator']['name'] = 'Production';
        $config['reroute_email.settings']['enable'] = FALSE;
        break;

    case 'test':
        $config['environment_indicator.indicator']['bg_color'] = '#0F8763';
        $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
        $config['environment_indicator.indicator']['name'] = 'Test';
        $config['reroute_email.settings']['enable'] = TRUE;
        $config['reroute_email.settings']['address'] = 'noreply@briefing.center';
        break;

    case 'dev':
        $config['environment_indicator.indicator']['bg_color'] = '#487391';
        $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
        $config['environment_indicator.indicator']['name'] = 'Development';
        $config['reroute_email.settings']['enable'] = TRUE;
        $config['reroute_email.settings']['address'] = 'noreply@briefing.center';
        break;

    case 'multidev':
        $config['environment_indicator.indicator']['bg_color'] = '#487391';
        $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
        $config['environment_indicator.indicator']['name'] = 'Development (Multidev)';
        $config['reroute_email.settings']['enable'] = TRUE;
        $config['reroute_email.settings']['address'] = 'noreply@briefing.center';
        break;

    case 'local':
    default:
        $config['environment_indicator.indicator']['bg_color'] = '#2B2B2D';
        $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
        $config['environment_indicator.indicator']['name'] = 'Local';
        $config['reroute_email.settings']['enable'] = TRUE;
        $config['reroute_email.settings']['address'] = 'noreply@briefing.center';
        break;
}