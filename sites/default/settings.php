<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all envrionments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to insure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

$settings['config_sync_directory'] = 'sites/default/config/sync';

$databases['policy_center_mysql']['default'] = array (
  'database' => 'News',
  'username' => 'root',
  'password' => '5XpJp6oDJTrJg8',
  'host' => 'database-1.ci5hq01ptzmo.us-east-1.rds.amazonaws.com',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_0900_ai_ci',
);

// Migrate boost settings.
$config['migrate_booster.settings']['modules'] = [
  'admin_toolbar_tools',
  'autosave_form',
  'pathauto',
  'search_api',
  'search_api_solr',
  'xmlsitemap',
];

// Migrate Scheduler module config
$config['migrate_scheduler']['migrations'] = [
  'policy_center_news' => [
    'time' => 3600,  # To be executed after every hour.
  ],
  'policy_center_feedly' => [
    'time' => 3600,  # To be executed every hour.
  ],
  'policy_center_feedly_press_media' => [
    'time' => 3600,  # To be executed every hour.
  ],
];

// Passthrough URL redirects
$uri = $_SERVER['REQUEST_URI'];
$url_to_match = '/passthrough?url=';
if( strpos( $uri, $url_to_match ) === 0) {

  $redirect_uri = str_replace( $url_to_match , "", $uri );
  if ( ( php_sapi_name() != "cli" ) ) {
    header( 'HTTP/1.0 301 Moved Permanently');
    header( 'Location: ' . $redirect_uri );

    if (extension_loaded('newrelic')) {
      newrelic_name_transaction("passthrough");
      newrelic_add_custom_parameter ('url', $redirect_uri);
    }

    exit();
  }

}
// Automatically generated include for settings managed by ddev.
$ddev_settings = dirname(__FILE__) . '/settings.ddev.php';
if (is_readable($ddev_settings) && getenv('IS_DDEV_PROJECT') == 'true') {
  require $ddev_settings;
}

// API Proxy settings for OrientDB integrations.
$config['api_proxy.settings']['api_proxies']['orient-db-api']['username'] = 'root';
$config['api_proxy.settings']['api_proxies']['orient-db-api']['password'] = 'policycenter';

/**
 * If there is a settings file to manage configuration, then include it.
 */
if (file_exists($app_root . '/' . $site_path . '/settings.config.php')) {
  include $app_root . '/' . $site_path . '/settings.config.php';
}
