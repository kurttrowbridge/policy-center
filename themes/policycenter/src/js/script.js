'use strict';

(function($) {
  $(document).ready(function(){

		// Get IE Version
		var version = detectIE();

		// import stylesheet for IE 11 and less
		if (version !== false && version <= 11) {
			$('head').append('<link rel="stylesheet" type="text/css" href="/sites/all/themes/raind/css/lte-ie11.css">');
		}

		// Detect IE
		// returns version of IE, or false if browser is not Internet Explorer
		function detectIE() {
			var ua = window.navigator.userAgent;

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// Edge (IE 12+) => return version number
				return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
			}
			// other browser
			return false;
		}

		// Skip Nav with tabindex fix
		$(".skip-link").click(function(event){

			// strip the leading hash and declare
			// the content we're skipping to
			var skipTo="#"+this.href.split('#')[1];

			// Setting 'tabindex' to -1 takes an element out of normal
			// tab flow but allows it to be focused via javascript
			$(skipTo).attr('tabindex', -1).on('blur focusout', function () {

				// when focus leaves this element,
				// remove the tabindex attribute
				$(this).removeAttr('tabindex');

			}).focus(); // focus on the content container
		});

		// Add SVG to body
		$.ajax({
			// Update url as needed
			url: "/themes/policycenter/images/svg-symbols.svg",
			context: document.body
		}).done(function (data) {
			$('svg', data).attr('class', 'visually-hidden').prependTo('body');
		});

		// Content cutoff
		var contentHeight = $('.content__story').height();
		console.log(contentHeight);
		if(contentHeight < 400) {
			$('.content__story').removeClass('long');
			$('#read-story').hide();
		}

		// Read more button on stories
		$('#read-story').on('click', function(e) {
			e.preventDefault();
			$(this).closest('.content').find('.content__story').addClass('open');
			$(this).hide();
		});

		// Search toggle
		$('.header__search-toggle').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$(this).closest('.header').next('.site-search').toggleClass('open');
		});


		// toggles
		$('.toggle-wrapper').on('click', '.toggle-label', function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$(this).next('.toggle-hidden').toggleClass('open');
			if($(this).hasClass('open')) {
				$(this).attr("aria-expanded", "true");
				$(this).next('.toggle-hidden').removeAttr('hidden');
			} else {
				$(this).attr("aria-expanded", "false");
				$(this).next('.toggle-hidden').attr('hidden', 'true');
			}
		});

		$(function() {
			var tabs = $("#topic-tabs");
		
			// For each individual tab DIV, set class and aria-hidden attribute, and hide it
			$(tabs).find("> div").attr({
				"class": "tabPanel",
				"aria-hidden": "true"
			}).hide();
		
			// Get the list of tab links
			var tabsList = tabs.find("ul:first").attr({
				"class": "topic-tabs__group",
			});
		
			// For each item in the tabs list...
			$(tabsList).find("li > a").each(
				function(a){
					var tab = $(this);
		
					// Create a unique id using the tab link's href
					var tabId = "tab-" + tab.attr("href").slice(1);
		
					// Assign tab id and aria-selected attribute to the tab control, but do not remove the href
					tab.attr({
						"id": tabId,
						"aria-selected": "false",
					}).parent().attr("role", "presentation");
		
					// Assign aria attribute to the relevant tab panel
					$(tabs).find(".tabPanel").eq(a).attr("aria-labelledby", tabId);
		
					// Set the click event for each tab link
					tab.click(
						function(e){
							var tabPanel;
		
							// Prevent default click event
							e.preventDefault();
		
							// Change state of previously selected tabList item
							$(tabsList).find("> li.current").removeClass("current").find("> a").attr("aria-selected", "false");
		
							// Hide previously selected tabPanel
							$(tabs).find(".tabPanel:visible").attr("aria-hidden", "true").hide();
		
							// Show newly selected tabPanel
							tabPanel = $(tabs).find(".tabPanel").eq(tab.parent().index());
							tabPanel.attr("aria-hidden", "false").show();
		
							// Set state of newly selected tab list item
							tab.attr("aria-selected", "true").parent().addClass("current");
		
							// Set focus to the first heading in the newly revealed tab content
							tabPanel.children("h2").attr("tabindex", -1).focus();
						}
					);
				}
			);
		
			// Set keydown events on tabList item for navigating tabs
			$(tabsList).delegate("a", "keydown",
				function (e) {
					var tab = $(this);
					switch (e.which) {
						case 37: case 38:
							if (tab.parent().prev().length!=0) {
								tab.parent().prev().find("> a").click();
							} else {
								$(tabsList).find("li:last > a").click();
							}
							break;
						case 39: case 40:
							if (tab.parent().next().length!=0) {
								tab.parent().next().find("> a").click();
							} else {
								$(tabsList).find("li:first > a").click();
							}
							break;
					}
				}
			);
		
			// Show the first tabPanel
			$(tabs).find(".tabPanel:first").attr("aria-hidden", "false").show();
		
			// Set state for the first tabsList li
			$(tabsList).find("li:first").addClass("current").find(" > a").attr({
				"aria-selected": "true",
				"tabindex": "0"
			});
		});

		// 2021 toggle
		$('.toggle-button').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$(this).next('.toggle-contents').toggleClass('open');
		});

		// Marketing page submenu
		$('.menu-expander').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('open');
			$(this).next('.menu__drawer').toggleClass('open');
		});
	});
})(jQuery);